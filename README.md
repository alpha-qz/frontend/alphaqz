<p align="center">
  <i>An open, extensible, Quiz Platform for your Class built using React and GoLang.</i>
  <br/>
</p>

![ظاهر برنامه](src/Assets/w5b7_screenshot_from_2020-03-13_17-59-27.png)

<p align="center">
  <a href="https://circleci.com/gh/outline/outline" rel="nofollow"><img src="https://img.shields.io/badge/license-MIT-green.svg"/></a>
  <a><img src="https://img.shields.io/badge/build-passing-brightgreen.svg" /></a>
  <a><img src="https://img.shields.io/badge/tests-477%20passed%2C%202%20failed-red.svg"/></a>
  <a><img src="https://img.shields.io/badge/tech%20debt-1%25-brightgreen.svg" /></a>
  <a><img src="https://img.shields.io/badge/stars-%E2%98%85%E2%98%85%E2%98%85%E2%98%85%E2%98%86-brightgreen.svg"/></a>
  <a href="https://eslint.org/"><img src="https://img.shields.io/badge/Linter-ESLint-ff69b4.svg?style=flat"></a>
  <a><img src="https://img.shields.io/badge/node%40latest-%3E%3D%206.0.0-brightgreen.svg" /> </a>
  <a><img src="https://img.shields.io/badge/PWA-brightgreen.svg" /> </a>
  <a href="https://github.com/prettier/prettier"><img src="https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat"></a>
  <a href="https://github.com/styled-components/styled-components"><img src="https://img.shields.io/badge/style-%F0%9F%92%85%20styled--components-orange.svg"></a>
</p>

This is the source code that runs [**AlphaQz**]().

If you'd like to run your own copy of AlphaQz or contribute to development then this is the place for you.

## Contributing

## License

No license is Available right now.
