// Route
export const authRoute = '/auth';
export const signUpRoute = `${authRoute}/signup`;
export const baseRoute = '/alpha';
export const landingRoute = '/';
export const classesRoute = `${baseRoute}/class`;
export const classRoute = `${baseRoute}/class/:class_id`;
export const examsRoute = `${classRoute}/exam`;
export const examRoute = `${classRoute}/test/:exam_id`;
export const createExamRoute = `${classRoute}/exam/create`;
export const examQuestionRoute = `${examRoute}/:question_id`;
export const questionsRoute = `${classRoute}/questions`;
export const aQuestionRoute = `${questionsRoute}/:question_id`;
export const createQuestionRoute = `${questionsRoute}/create`;
export const myExamsRoute = `${classRoute}/exam/myexam`;
export const editExamRoute = `${classRoute}/exam/create/:exam_id`;
export const myQuestionsRoute = `${questionsRoute}/myquestions`;
export const editQuestionRoute = `${questionsRoute}/create/:question_id`;

// Submissions Routes
export const mySubmissionsRoute = `${classRoute}/submissions`;
export const mySubmissionRoute = `${classRoute}/submissions/:exam_id`;

// Link

export const authLink = '/auth';
export const signUpLink = `${authLink}/signup`;
export const baseLink = '/alpha';
export const landingLink = '/';
export const classesLink = `${baseLink}/class`;
export const classLink = class_id => `${baseLink}/class/${class_id}`;
export const examsLink = class_id => `${classLink(class_id)}/exam`;
export const examLink = (class_id, exam_id) =>
    `${classLink(class_id)}/test/${exam_id}`;
export const createExamLink = class_id => `${classLink(class_id)}/exam/create`;
export const editExamLink = (class_id, exam_id) =>
    `${classLink(class_id)}/exam/create/${exam_id}`;
export const examQuestionLink = (class_id, exam_id, question_id) =>
    `${classLink(class_id)}/test/${exam_id}/${question_id}`;
export const questionsLink = class_id => `${classLink(class_id)}/questions`;
export const aQuestionLink = (class_id, question_id) =>
    `${questionsLink(class_id)}/${question_id}`;
export const createQuestionLink = class_id =>
    `${questionsLink(class_id)}/create`;
export const myExamsLink = class_id => `${classLink(class_id)}/exam/myexam`;
export const editQuestionLink = (class_id, question_id) =>
    `${questionsLink(class_id)}/create/${question_id}`;
export const myQuestionsLink = class_id =>
    `${questionsLink(class_id)}/myquestions`;

// Submissions Link
export const mySubmissionsLink = class_id =>
    `${classLink(class_id)}/submissions`;
export const mySubmissionLink = (class_id, exam_id) =>
    `${classLink(class_id)}/submissions/${exam_id}`;
