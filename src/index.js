import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import Landing from './Components/Landing';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import rootReducer from './Reducers';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import AuthController from './Containers/AuthController';
import TestApi from './Containers/TestApi';
import MainLayoutContainer from './Containers/MainLayoutContainer';
import * as Routes from './Constants/Routes/Routes';
import App from './App';

const store = createStore(rootReducer);
let baseUrl = '';
if (process.env.NODE_ENV === 'development') {
    baseUrl = '';
}

if (process.env.NODE_ENV === 'production') {
    baseUrl = '';
}

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter basename={baseUrl}>
            <Switch>
                <Route exact path={'/test'} component={TestApi} />
                <Route path={Routes.authRoute} component={AuthController} />
                <Route
                    exact
                    path={Routes.baseRoute}
                    component={MainLayoutContainer}
                />
                <Route path={Routes.classesRoute} component={App} />
                <Route exact path={Routes.landingRoute} component={Landing} />
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
);
