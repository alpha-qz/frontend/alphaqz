import React from 'react';
import {TimePicker} from 'antd';
import './FormInput.css';
import moment from 'moment';

export default props => {
    const onChange = (date, dateString) => {
        console.log(dateString);
        props.getInputValue(dateString);
    };
    let time;
    if (props.defaultValue)
        time = moment(props.defaultValue.split('T')[1].slice(0, 8), 'HH:mm:ss');

    return (
        <>
            <label className={'form_label'}>
                <p>{props.label}</p>
            </label>
            <div className={'form_clock'}>
                <TimePicker
                    defaultValue={time}
                    onChange={onChange}
                    style={{width: '100%', direction: 'ltr !important'}}
                    size={'large'}
                />
            </div>
        </>
    );
};
