import React from 'react';
import {Input} from 'antd';
import './FormInput.css';

export default props => (
    <>
        <label className={'form_label'}>
            <p>{props.label}</p>
        </label>
        <Input
            onChange={e => props.getInputValue(e.target.value)}
            defaultValue={props.defaultValue}
            className={'form_input'}
            size={'large'}
            placeholder={props.placeholder}
        />
    </>
);
