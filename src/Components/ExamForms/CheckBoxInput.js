import React from 'react';
import {Checkbox} from 'antd';
import './FormInput.css';

export default function CheckBoxInput(props) {
    const onChange = value => {
        if (value.length) {
            props.getInputValue(true);
        } else {
            props.getInputValue(false);
        }
    };
    return (
        <div>
            <label className={'form_label'}>
                <p>{props.label}</p>
            </label>
            <div className={'checkbox_wrapper form_input'}>
                <Checkbox.Group
                    onChange={onChange}
                    options={props.options}
                    defaultValue={[props.defaultValue ? props.options[0] : '']}
                />
            </div>
        </div>
    );
}
