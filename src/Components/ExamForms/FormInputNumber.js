import React from 'react';
import {InputNumber} from 'antd';
import './FormInput.css';

export default props => (
    <>
        <label className={'form_label'}>
            <p>{props.label}</p>
        </label>
        <div className="form_input">
            <InputNumber
                min={props.min}
                max={props.max}
                size={'large'}
                onChange={e => props.getInputValue(e)}
                defaultValue={props.defaultValue}
            />
        </div>
    </>
);
