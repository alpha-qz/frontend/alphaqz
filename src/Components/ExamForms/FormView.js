import React, {useState, useEffect} from 'react';
import {Row, Col, Button} from 'antd';
import './FormView.css';
import FormInput from './FormInput';
import CheckBoxInput from './CheckBoxInput';
import FormInputNumber from './FormInputNumber';
import FormTimeInput from './FormTimeInput';
import FormDateInput from './FormDateInput';

let formData = {
    name: null,
    is_public: false,
    begin_time: null,
};

export default function FormView(props) {
    const [examTime, setExamTime] = useState('');
    const [examData, setExamDate] = useState('');
    const plainOptions = ['آزمون به صورت عمومی ساخته شود.'];
    const handleFormData = (key, value) => {
        formData = {
            ...formData,
            [key]: String(value),
        };
    };

    const onSetExamTime = () => {
        formData = {
            ...formData,
            begin_time: `${examData}T${examTime}`,
        };
        console.log(formData);
    };
    let exam = props.exam;
    useEffect(() => {
        if (Object.keys(exam).length) {
            formData = {...exam};
            let beginTime = exam['begin_time'].split('T');
            let date = beginTime[0];
            setExamDate(date);
            let time = beginTime[1];
            setExamTime(time);
        }
    }, []);

    return (
        <div className={'form_wrapper'}>
            <Row gutter={[16, 16]}>
                <Col span={12}>
                    <div className={'info'}>
                        <h3 dir={'rtl'}> توضیحات</h3>
                        <p className={'desc'}>
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                            صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و
                            متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم
                            است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای
                            متنوع با هدف بهبود ابزارهای کاربردی می باشد
                        </p>
                        <br />
                        <h3 dir={'rtl'}> توضیحات</h3>
                        <p className={'desc'}>
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                            صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها
                            و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که
                            لازم است
                        </p>
                        <br />
                        <h3 dir={'rtl'}> توضیحات</h3>
                        <p className={'desc'}>
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                            صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و
                            متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم
                            است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای
                            متنوع با هدف بهبود ابزارهای کاربردی می باشد
                        </p>
                        <br />
                        <h3 dir={'rtl'}> توضیحات</h3>
                        <p className={'desc'}>
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                            صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها
                            و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که
                            لازم است
                        </p>
                    </div>
                </Col>
                <Col span={12}>
                    <Row>
                        <Col span={24}>
                            <FormInput
                                placeholder={
                                    'مثال:میان ترم دوم سیگنال ها و سیستم ها'
                                }
                                getInputValue={value =>
                                    handleFormData('name', value)
                                }
                                label={'نام آزمون'}
                                defaultValue={exam['name']}
                            />
                        </Col>
                    </Row>
                    <br />
                    <br />
                    <Row gutter={[16, 40]} dir={'rtl'}>
                        <CheckBoxInput
                            options={plainOptions}
                            label={''}
                            getInputValue={value =>
                                handleFormData('is_public', value)
                            }
                            defaultValue={exam['is_public']}
                        />
                    </Row>
                    <br />
                    <br />
                    <Row gutter={[16, 40]}>
                        <Col span={12}>
                            <FormDateInput
                                label={'تاریخ برگزاری آزمون'}
                                getInputValue={value => setExamDate(value)}
                                defaultValue={exam['begin_time']}
                            />
                        </Col>
                        <Col span={12}>
                            <FormTimeInput
                                defaultValue={exam['begin_time']}
                                getInputValue={value => setExamTime(value)}
                                label={'ساعت برگزاری آزمون'}
                            />
                        </Col>
                    </Row>
                    <Button
                        onClick={async () => {
                            await onSetExamTime();
                            props.createExam(formData);
                        }}
                        type={'primary'}
                    >
                        {props.btnText}
                    </Button>
                </Col>
            </Row>
        </div>
    );
}

FormView.defaultProps = {
    btnText: 'ساخت آزمون جدید',
};
