import React from 'react';
import './footer.css';

export default function StatusFooter(props) {
    return (
        <div className={'online_exam_footer'}>
            <div className={'online_exam_wrapper'}>{props.children}</div>
        </div>
    );
}
