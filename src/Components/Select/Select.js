import React, {useState} from 'react';
import {Select, Button} from 'antd';
import './Select.css';
const {Option} = Select;

export default function CustomSelect(props) {
    const [id, setId] = useState(null);
    const handleChange = value => {
        setId(value);
    };

    const handleClick = () => {
        props.onClick(id);
    };

    const defaultValue = props.data[props.defaultValue]
        ? props.data[props.defaultValue].title
        : '';

    return (
        <>
            <Button onClick={handleClick} className={'select_filter_btn'}>
                {props.btnText}
            </Button>
            <Select
                defaultValue={defaultValue}
                style={{width: 240}}
                onChange={handleChange}
                loading={props.loading}
            >
                {props.data.map(item => (
                    <Option key={item.id} value={item.id}>
                        {item.name}
                    </Option>
                ))}
            </Select>
        </>
    );
}
CustomSelect.defaultProps = {
    data: [],
};
