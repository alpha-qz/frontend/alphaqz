import React from 'react';
import {Layout} from 'antd';
import './Sidebar/Sidebar.css';
import LayoutHeader from './LayoutHeader';

const {Content, Footer} = Layout;

export default props => (
    <Layout className="site-layout" style={{marginRight: 300}}>
        <LayoutHeader />
        <Content
            style={{
                margin: '24px 16px 0',
                overflow: 'initial',
                ...props.contentStyle,
            }}
        >
            <div
                className="site-layout-background layout_content"
                style={{padding: 24, height: '100%'}}
            >
                {props.children}
            </div>
        </Content>
        <Footer>{props.footer}</Footer>
    </Layout>
);
