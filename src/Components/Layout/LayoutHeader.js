import React from 'react';
import {Layout, Button, notification} from 'antd';
import {Clock} from "../UI's/Clock/Clock";
import ErrorHandler from '../../Lib/ErrorHandler';
import ResponseHandler from '../../Lib/ResponseHandler';
import {logoutAccount} from '../../Lib/ApiService/Private';

const {Header} = Layout;

export default props => {
    const handleClick = () => {
        logoutAccount()
            .then(async response => {
                let res = ResponseHandler(response);
                notification[res.type]({
                    message: 'شما با موفقیت خارج شدید.',
                    description: res.serverMessage,
                    duration: 3,
                });
                sessionStorage.clear();
                window.location.reload();
            })
            .catch(err => {
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
    };

    return (
        <Header
            className="site-layout-background layout_header"
            style={{padding: 0}}
        >
            <div>
                <Button type="link" onClick={handleClick}>
                    خروج
                </Button>
            </div>
            <div>
                <Clock />
            </div>
            <div>search bar</div>
        </Header>
    );
};
