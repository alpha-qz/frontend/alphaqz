import React from 'react';
import {Menu} from 'antd';
import {Link} from 'react-router-dom';

export default props => {
    return (
        <Menu
            theme="light"
            mode="inline"
            defaultSelectedKeys={[props.defaultSelectedKey]}
        >
            {props.links.map(item => (
                <Menu.Item key={item.key} style={props.itemStyles}>
                    {item.icon}
                    <Link to={item.path}>
                        <span className="nav-text">{item.label}</span>
                    </Link>
                </Menu.Item>
            ))}
        </Menu>
    );
};
