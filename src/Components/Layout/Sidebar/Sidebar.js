import React from 'react';
import {Layout} from 'antd';
import SidebarItems from './SidebarItems';
import SidebarHeader from './SidebarHeader';
import './Sidebar.css';

const {Sider} = Layout;

export default props => (
    <Sider
        style={{
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            right: 0,
            flex: '0 0 300px',
            maxWidth: '300px',
            minWidth: '300px',
            width: '300px',
            boxShadow: '-5px 0px 5px #cecece',
        }}
    >
        <SidebarHeader />
        <SidebarItems
            itemStyles={props.itemStyles}
            links={props.links}
            defaultSelectedKey={props.defaultSelectedKey}
        />
    </Sider>
);
