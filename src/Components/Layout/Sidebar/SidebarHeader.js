import React from 'react';
import logo from '../../../Assets/logo.png';
import Badge from "../../UI's/Badge/Badge";
import {Link} from 'react-router-dom';
import * as Routes from '../../../Constants/Routes/Routes';

export default props => (
    <>
        <div className="logo">
            <img src={logo} alt={'An error occurred while loading'} />
            <p>
                <Link to={Routes.landingLink}>Alpha QZ</Link>
            </p>
            <Badge
                title={'نسخه آزمایشی'}
                bgColor={'rgba(0, 0, 0, 0.1)'}
                color={'white'}
            />
        </div>
        <div className={'children_content'}>{props.children}</div>
    </>
);
