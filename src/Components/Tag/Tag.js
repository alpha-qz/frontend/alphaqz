import React from 'react';
import './Tag.css';
export default function Tag(props) {
    const getBgColor = bgColor => {
        switch (bgColor) {
            case 'success':
                return '#008274';
            case 'warning':
                return 'orange';
            case 'danger':
                return 'red';
            case 'info':
                return 'blue';
        }
    };

    return (
        <span
            className={'Tag_'}
            style={{backgroundColor: getBgColor(props.type)}}
        >
            {props.children}
        </span>
    );
}
