import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import * as Routes from '../../Constants/Routes/Routes';
import './AddQuestionHint.css';
import cactus from '../../Assets/cactus.png';
function AddQuestionHint(props) {
    const classId = props.match.params.class_id;
    return (
        <div className={'add_question_hint'}>
            <img src={cactus} />
            <Link to={Routes.questionsLink(classId)}>
                <p>سوالات خود را به امتحان اضافه کنید.</p>
            </Link>
        </div>
    );
}
export default withRouter(AddQuestionHint);
