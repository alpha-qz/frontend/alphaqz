import React from 'react';
import persianJs from 'persianjs';

export default function PersianDigit(props) {
    return <>{String(persianJs(props.children).englishNumber())}</>;
}
