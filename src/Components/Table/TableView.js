import React from 'react';
import {Table} from 'antd';
import './Table.css';

export default props => (
    <div className={'table_wrapper'}>
        <Table {...props} />
    </div>
);
