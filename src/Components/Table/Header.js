import React from 'react';
import './Header.css';

export default props => (
    <>
        <div className={'border_top'}></div>
        <div className={'wrapper'}>
            <p>{props.title}</p>
        </div>
    </>
);
