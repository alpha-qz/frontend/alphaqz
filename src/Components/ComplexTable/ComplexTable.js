import React, {useState, useRef} from 'react';
import {withRouter, Link} from 'react-router-dom';
import * as Routes from '../../Constants/Routes/Routes';
import {Table, Input, Button, Tag} from 'antd';
import Highlighter from 'react-highlight-words';
import {SearchOutlined} from '@ant-design/icons';
import persianJs from 'persianjs';
import '../../Components/specialStyles/QuestionTable.css';

function ComplexTable(props) {
    const classId = props.classId;

    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    let searchInput = useRef(null);

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = clearFilters => {
        clearFilters();
        setSearchText('');
    };

    const getTagColor = () => {
        return '#008274';
    };

    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div style={{padding: 8}}>
                <Input
                    ref={node => {
                        searchInput = node;
                    }}
                    placeholder={`جستجو`}
                    value={selectedKeys[0]}
                    onChange={e =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    style={{width: 188, marginBottom: 8, display: 'block'}}
                />
                <Button
                    type="primary"
                    onClick={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    icon={''}
                    size="small"
                    style={{width: 90, marginRight: 8}}
                >
                    <SearchOutlined />
                    جستجو
                </Button>
                <Button
                    onClick={() => handleReset(clearFilters)}
                    size="small"
                    style={{width: 90}}
                >
                    حذف
                </Button>
            </div>
        ),
        filterIcon: filtered => (
            <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}} />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => searchInput.select());
            }
        },
        render: text =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text.toString()}
                />
            ) : (
                text
            ),
    });

    const getQuestionLevel = option => {
        return option;
    };

    const getDifficulty = difficulty => {
        const level = String(difficulty).replace(/\D/g, '');
        switch (level) {
            case '10':
            case '9':
            case '8':
                return <Tag color="volcano">خیلی سخت</Tag>;
            case '7':
            case '6':
            case '5':
                return <Tag color="orange">سخت</Tag>;
            case '4':
            case '3':
                return <Tag color="lime">متوسط</Tag>;
            case '2':
            case '1':
                return <Tag color="green">آسان</Tag>;
        }
    };

    const columns = [
        ...Object.keys(props.actions).map(item => {
            return {
                title: '',
                dataIndex: item,
                key: item,
                render: (action, record) => (
                    <Button
                        className={'table_button'}
                        size={'small'}
                        onClick={() => props.actions[item][1](record.key)}
                    >
                        {props.actions[item][0]}
                    </Button>
                ),
                width: '1%',
            };
        }),
        {
            title: 'سطح سوال',
            dataIndex: 'difficulty',
            key: 'difficulty',
            width: '10%',
            ...getColumnSearchProps('difficulty'),
            render: difficulty => getDifficulty(difficulty),
        },
        {
            title: 'برچسب',
            dataIndex: 'tag',
            key: 'tag',
            width: '20%',
            ...getColumnSearchProps('tag'),
            render: tags => (
                <span>
                    {tags.map(tag => {
                        let color = getTagColor(tag);
                        return (
                            <Tag color={color} key={tag}>
                                {tag.toUpperCase()}
                            </Tag>
                        );
                    })}
                </span>
            ),
        },
        {
            title: 'سوال',
            dataIndex: 'title',
            key: 'title',
            width: '20%',
            ...getColumnSearchProps('title'),
            render: (title, record) => (
                <Button type="link">
                    <Link
                        to={Routes.aQuestionLink(
                            classId,
                            persianJs(record.id).persianNumber(),
                        )}
                    >
                        {title}
                    </Link>
                </Button>
            ),
        },
        {
            title: 'شناسه',
            dataIndex: 'id',
            key: 'id',
            width: '8%',
            ...getColumnSearchProps('id'),
        },
    ];
    let content = [
        {
            id: '۱',
            title: 'انتگرال دو گانه',
            tag: ['ریاضیات', 'آمار و احتمالات'],
            used: '۵۲۴',
            key: 1,
        },
        {
            id: '۲',
            title: 'سری فیبوناچی',
            tag: ['ریاضیات'],
            used: '۱۳۵۱',
            key: 2,
        },
        {
            id: '۳',
            title: 'انتگرال سه گانه',
            tag: ['ریاضیات'],
            used: '۳۶۸۶',
            key: 3,
        },
        {
            id: '۴',
            title: 'آمار و احتمالات',
            tag: ['ریاضیات', 'علوم داده'],
            used: '۸۶۴',
            key: 5,
        },
    ];

    if (!props.loading && !props.error) {
        const {type} = props;
        content = props.data[type].edges.map(node => {
            if (node.node === null) return null;
            let id = String(
                persianJs(
                    atob(node.node.id)
                        .split(':')
                        .slice(-1)[0],
                ).englishNumber(),
            );
            return {
                id: id,
                title: node.node.title,
                tag: node.node.qtag.edges.map(subNode => {
                    return subNode.node.tag.title;
                }),
                difficulty: getQuestionLevel(node.node.difficulty),
                key: atob(node.node.id)
                    .split(':')
                    .slice(-1)[0],
            };
        });
    }
    return (
        <>
            <Table
                loading={props.loading}
                columns={columns}
                dataSource={content}
            />
        </>
    );
}

ComplexTable.defaultProps = {
    actions: {},
    type: 'allQuestions',
};

export default withRouter(ComplexTable);
