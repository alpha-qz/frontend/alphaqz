import React from 'react';
import {Modal} from 'antd';
import './Modal.css';

export default function CustomModal(props) {
    const handleOk = () => {
        props.handleOk();
    };

    const handleCancel = () => {
        props.onClose();
    };

    return (
        <div>
            <Modal
                title={props.title}
                cancelText={props.cancelText}
                visible={props.visible}
                confirmLoading={true}
                onCancel={handleCancel}
                footer={props.footer}
            >
                <div dir={'rtl'}>{props.children}</div>
            </Modal>
        </div>
    );
}
