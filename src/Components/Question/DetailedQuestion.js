import React from 'react';
import './Qeustion.css';

function DetailedQuestion(props) {
    return (
        <p
            className={'q_description'}
            dangerouslySetInnerHTML={{
                __html: props.question['description'],
            }}
        ></p>
    );
}

export default DetailedQuestion;
