import React, {useState, useEffect} from 'react';
import {Radio} from 'antd';
import './Qeustion.css';

function TestQuestion(props) {
    const [value, changeRadios] = useState();
    const [options, setOptions] = useState([]);
    const {defualtAnswer} = props;
    useEffect(() => {
        createOptionsObject();
        changeRadios(defualtAnswer);
    }, []);

    const createOptionsObject = () => {
        const {question} = props;
        let options = Object.keys(question.options).map((key, idx) => ({
            label: question.options[key],
            value: key,
        }));
        setOptions(options);
    };

    const handleRadio = e => {
        changeRadios(String(e.target.value));
        props.onChange(e.target.value);
    };

    return (
        <>
            <h5 dir={'rtl'} className={'q_description text_color'}>
                <div
                    dangerouslySetInnerHTML={{
                        __html: props.question['description'],
                    }}
                ></div>
            </h5>
            <div dir={'rtl'}>
                <Radio.Group
                    options={options}
                    className={'radio_group'}
                    onChange={handleRadio}
                    value={value}
                />
            </div>
        </>
    );
}

export default TestQuestion;
