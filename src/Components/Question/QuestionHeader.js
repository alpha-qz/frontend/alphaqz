import React from 'react';
import CountDownTime from "../../Components/UI's/CountDownTimer/CountDownTimer";
import './Qeustion.css';

export default function QuestionHeader(props) {
    return (
        <>
            <div className={'q_header_title'}>
                {props.OnlineTest ? (
                    <h5 className={'q_timer'}>
                        <CountDownTime date={'2020-03-30 19:00:00'} />
                    </h5>
                ) : (
                    <h5 className={'q_timer'}>{props.qAuthor}</h5>
                )}
                <h4 className={'text_color q_title'}>{props.qLabel}</h4>
                <h5 className={'q_course_title'}>{props.cLabel}</h5>
            </div>
            <hr />
        </>
    );
}
