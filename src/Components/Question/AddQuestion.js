import React, {useState} from 'react';
import Spin from "../../Components/UI's/Spin/Spin";
import './AddQuestion.css';
import {Button, Tooltip} from 'antd';
import {PlusCircleOutlined} from '@ant-design/icons';
import FormInputNumber from '../QuestionForms/FormInputNumber';

export default function AddQuestion(props) {
    let point = 50;
    const handleFormData = value => {
        point = value;
    };
    const {examsList} = props;
    return (
        <>
            <h3>اضافه کردن سوال به امتحان</h3>
            <hr className={'normal_hr_grey'} />
            <Spin loading={props.loading}>
                {examsList.map(exam => (
                    <div
                        key={exam['id']}
                        className={'add_question_row_wrapper'}
                    >
                        <p>{exam['name']}</p>
                        <Tooltip title="اضافه کردن به امتحان">
                            <Button
                                type="link"
                                onClick={() =>
                                    props.addQuestion(
                                        props.questionId,
                                        exam['id'],
                                        point,
                                    )
                                }
                                shape="circle"
                                icon={<PlusCircleOutlined />}
                            />
                        </Tooltip>
                    </div>
                ))}
                <div style={{width: '150px'}}>
                    <FormInputNumber
                        getInputValue={value => handleFormData(value)}
                        defaultValue={50}
                        size={'medium'}
                        label={'نمره سوال'}
                        min={1}
                        max={100}
                    />
                </div>
            </Spin>
        </>
    );
}
