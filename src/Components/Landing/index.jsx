import React from 'react';

import Header from './Header/Header';
import Banner from './Banner/Banner';
import Page1 from './Pages/Page1';
import Page2 from './Pages/Page2';
import Page3 from './Pages/Page3';
import Footer from './Footer/Footer';
import './index.css';

export default () => (
    <div className="page-wrapper home">
        <Header />
        <Banner />
        <Page1 />
        <Page2 />
        <Page3 />
        <Footer />
    </div>
);
