import React from 'react';
import {Row, Col} from 'antd';

function Footer() {
    return (
        <footer id="footer" className="dark">
            <div className="footer-wrap">
                <Row>
                    <Col md={6} sm={24} xs={24}>
                        <div className="footer-center">
                            <h2>Alpha QZ</h2>
                            <div>
                                <a target="_blank " href="https://example.com">
                                    GitHub
                                </a>
                            </div>
                            <div>
                                <a target="_blank " href="https://example.com">
                                    <p>MIT License</p>
                                </a>
                            </div>
                        </div>
                    </Col>
                    <Col md={6} sm={24} xs={24}>
                        <div className="footer-center">
                            <h2>
                                <p>تماس با ما</p>
                            </h2>
                        </div>
                    </Col>
                    <Col md={6} sm={24} xs={24}>
                        <div className="footer-center">
                            <h2>
                                <p>درباره ما</p>
                            </h2>
                        </div>
                    </Col>
                    <Col md={6} sm={24} xs={24}>
                        <div className="footer-center">
                            <h2>
                                <p>همکاری با ما</p>
                            </h2>
                        </div>
                    </Col>
                </Row>
            </div>
            <Row className="bottom-bar">
                <Col md={4} sm={24}></Col>
                <Col md={20} sm={24}></Col>
            </Row>
        </footer>
    );
}

export default Footer;
