import React from 'react';
import './Header.css';
export default function Header(props) {
    return (
        <header {...props} id="header">
            <a id="logo" href={'https://example.com'}>
                <img
                    alt="logo"
                    src="https://www.freelogoservices.com/blog/wp-content/uploads/transparent-logo.jpg"
                />
            </a>
            <p className={'header_title'}>Alpha QZ</p>
        </header>
    );
}
