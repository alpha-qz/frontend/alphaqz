import React from 'react';
import PropTypes from 'prop-types';
import TweenOne from 'rc-tween-one';
import QueueAnim from 'rc-queue-anim';
import ScrollParallax from 'rc-scroll-anim/lib/ScrollParallax';
import BannerImage from './BannerImage';
import './Banner.css';
import {Button} from 'antd';
import {Link} from 'react-router-dom';
import * as Routes from '../../../Constants/Routes/Routes';

const loop = {
    duration: 3000,
    yoyo: true,
    repeat: -1,
};

class Banner extends React.PureComponent {
    static propTypes = {
        className: PropTypes.string,
    };
    static defaultProps = {
        className: 'banner',
    };

    render() {
        const {className, isMobile} = this.props;
        return (
            <div className="home-page-wrapper banner-wrapper" id="banner">
                <div className="banner-bg-wrapper">
                    <svg
                        width="400px"
                        height="576px"
                        viewBox="0 0 400 576"
                        fill="none"
                    >
                        <TweenOne
                            component="g"
                            animation={[
                                {opacity: 0, type: 'from'},
                                {...loop, y: 15},
                            ]}
                        >
                            <ellipse
                                id="Oval-9-Copy-4"
                                cx="100"
                                cy="100"
                                rx="6"
                                ry="6"
                                stroke="#2F54EB"
                                strokeWidth="1.6"
                            />
                        </TweenOne>
                        <TweenOne
                            component="g"
                            animation={[
                                {opacity: 0, type: 'from'},
                                {...loop, y: -15},
                            ]}
                        >
                            <g transform="translate(200 400)">
                                <g
                                    style={{
                                        transformOrigin: '50% 50%',
                                        transform: 'rotate(-340deg)',
                                    }}
                                >
                                    <rect
                                        stroke="#FADB14"
                                        strokeWidth="1.6"
                                        width="9"
                                        height="9"
                                    />
                                </g>
                            </g>
                        </TweenOne>
                    </svg>
                    <ScrollParallax
                        location="banner"
                        className="banner-bg"
                        animation={{playScale: [1, 1.5], rotate: 0}}
                    />
                </div>
                <QueueAnim
                    className={`${className} page`}
                    type="alpha"
                    delay={150}
                >
                    {isMobile && (
                        <div className="img-wrapper" key="image">
                            <BannerImage />
                        </div>
                    )}
                    <QueueAnim
                        className="text-wrapper"
                        key="text"
                        type="bottom"
                    >
                        <h1 className={'banner_header'} key="h1">
                            سامانه برگزاری و تصحیح آزمون های آنلاین
                        </h1>
                        <p className={'banner_header_subtitle'} key="p">
                            کلاس درس دلخواه خودتان را رایگان ایجاد کنید !
                        </p>
                        <Link to={Routes.classesLink}>
                            <Button type="primary">شروع کنید!</Button>
                        </Link>
                    </QueueAnim>

                    <div className="img-wrapper" key="image">
                        <ScrollParallax
                            location="banner"
                            component={BannerImage}
                            animation={{playScale: [1, 1.5], y: 80}}
                        />
                    </div>
                </QueueAnim>
            </div>
        );
    }
}

export default Banner;
