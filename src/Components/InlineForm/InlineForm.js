import React, {useState, useEffect} from 'react';
import {Form, Input, Button} from 'antd';
import './InlineForm.css';

export default function HorizontalLoginForm(props) {
    const [form] = Form.useForm();
    const [, forceUpdate] = useState();

    useEffect(() => {
        forceUpdate({});
    }, []);

    const onFinish = values => {
        Object.keys(values).map(item =>
            values[item] === undefined ? (values[item] = '') : null,
        );
        props.handleFilterData(values);
    };

    return (
        <Form
            form={form}
            size="large"
            style={{direction: 'rtl'}}
            name="filter"
            layout="inline"
            onFinish={onFinish}
        >
            {props.forms.map(item => (
                <Form.Item
                    key={item.key}
                    name={item.name}
                    size={item.size}
                    rules={[
                        {
                            required: item.required,
                            message: item.message,
                        },
                    ]}
                >
                    <Input
                        className={'inline__form'}
                        type={item.type}
                        placeholder={item.placeholder}
                    />
                </Form.Item>
            ))}
            <Form.Item shouldUpdate>
                {() => (
                    <Button
                        className={'inline_form_btn'}
                        type="primary"
                        htmlType="submit"
                        loading={props.loading}
                    >
                        {props.filterState ? 'حذف فیلتر' : 'اعمال فیلتر'}
                    </Button>
                )}
            </Form.Item>
        </Form>
    );
}
