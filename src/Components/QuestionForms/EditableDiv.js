import React, {useState} from 'react';
import {Input} from 'antd';
import './EditableDiv.css';
import {QuestionCircleOutlined} from '@ant-design/icons';
import './FormInput.css';

export default function EditableDiv(props) {
    const [options, setOptions] = useState(Object.values(props.content));
    const onChange = ({target: {value}}, idx) => {
        let newOptions = [...options];
        newOptions[idx] = value;
        setOptions(newOptions);
        props.getInputValue(newOptions);
    };
    return Object.keys(props.content).map((item, idx) => {
        return (
            <div key={item}>
                <p className={'choice_label'}>
                    <QuestionCircleOutlined />
                    {item}
                </p>
                <Input
                    onChange={e => onChange(e, idx)}
                    defaultValue={props.content[item]}
                    className={'form_input choice_input'}
                    size={'medium'}
                />
            </div>
        );
    });
}
