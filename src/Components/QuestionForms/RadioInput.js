import React from 'react';
import {Radio} from 'antd';
import './FormInput.css';

export default function RadioInput(props) {
    const onChange = e => {
        props.setOption(e.target.value);
    };
    return (
        <>
            <label className={'form_label'}>
                <p>{props.label}</p>
            </label>
            <div className={'checkbox_wrapper form_input'}>
                <Radio.Group value={props.value} onChange={onChange}>
                    {Object.keys(props.options).map(item => (
                        <Radio key={item} value={item}>
                            {props.options[item]}
                        </Radio>
                    ))}
                </Radio.Group>
            </div>
        </>
    );
}
