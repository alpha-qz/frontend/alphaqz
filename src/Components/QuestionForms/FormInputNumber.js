import React from 'react';
import {InputNumber} from 'antd';
import './FormInput.css';

export default props => {
    const onChange = e => {
        props.getInputValue(e);
    };
    return (
        <>
            <label className={'form_label'}>
                <p>{props.label}</p>
            </label>
            <div className="form_input">
                <InputNumber
                    onChange={onChange}
                    min={props.min}
                    max={props.max}
                    size={props.size || 'large'}
                    defaultValue={props.defaultValue}
                />
            </div>
        </>
    );
};
