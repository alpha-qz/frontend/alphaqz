import React from 'react';
import {TimePicker} from 'antd';
import './FormInput.css';

export default props => (
    <>
        <label className={'form_label'}>
            <p>{props.label}</p>
        </label>
        <div className={'form_clock'}>
            <TimePicker
                style={{width: '100%', direction: 'ltr !important'}}
                size={'large'}
            />
        </div>
    </>
);
