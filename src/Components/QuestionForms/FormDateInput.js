import React from 'react';
import './FormInput.css';
import {DatePicker} from 'antd';
import {ConfigProvider} from 'antd';
import moment from 'jalali-moment';
import 'moment/locale/fa';

moment.locale('fa');

const config = {
    lang: {
        locale: 'fa_IR',
        placeholder: 'انتخاب تاریخ',
        rangePlaceholder: ['Start date', 'End date'],
        today: 'Today',
        now: 'Now',
        backToToday: 'Back to today',
        ok: 'Ok',
        clear: 'Clear',
        month: 'Month',
        year: 'Year',
        timeSelect: 'Select time',
        dateSelect: 'Select date',
        monthSelect: 'Choose a month',
        yearSelect: 'Choose a year',
        decadeSelect: 'Choose a decade',
        yearFormat: 'YYYY',
        dateFormat: 'M/D/YYYY',
        dayFormat: 'D',
        dateTimeFormat: 'M/D/YYYY HH:mm:ss',
        monthFormat: 'MMMM',
        monthBeforeYear: true,
        previousMonth: 'Previous month (PageUp)',
        nextMonth: 'Next month (PageDown)',
        previousYear: 'Last year (Control + left)',
        nextYear: 'Next year (Control + right)',
        previousDecade: 'Last decade',
        nextDecade: 'Next decade',
        previousCentury: 'Last century',
        nextCentury: 'Next century',
    },
};

export default props => {
    return (
        <>
            <label className={'form_label'}>
                <p>{props.label}</p>
            </label>
            <div className={'form_clock'}>
                <ConfigProvider locale={config}>
                    <DatePicker
                        defaultPickerValue={moment()}
                        locale={config}
                        style={{width: '100%', direction: 'ltr !important'}}
                        size={'large'}
                    />
                </ConfigProvider>
            </div>
        </>
    );
};
