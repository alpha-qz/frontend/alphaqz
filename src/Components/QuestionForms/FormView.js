import React, {useState, useEffect} from 'react';
import {Row, Col, Button} from 'antd';
import './FormView.css';
import FormInput from './FormInput';
import FormInputNumber from './FormInputNumber';
import CheckBoxInput from './CheckBoxInput';
import Editor from "../UI's/Editor/Editor";
import RadioInput from './RadioInput';
import EditableDiv from './EditableDiv';
import QuestionTagContainer from '../../Containers/Question/QuestionTagContainer';
import FormTextArea from './FormTextArea';

let formData = {
    title: null,
    questionText: null,
    answer: null,
    comments: null,
    difficulty: null,
    isPublic: null,
    rightChoice: 1,
    choiceset: [],
    categories: [],
    tags: [],
};

export default function FormView(props) {
    const [type, setType] = useState('test');
    const plainOptions = {test: 'تستی', detailed: 'تشریحی'};
    const addToPublic = ['به مخزن سوالات اضافه شود'];
    const setOption = choice => setType(choice);
    let content = {
        'گزینه اول': '',
        'گزینه دوم': '',
        'گزینه سوم': '',
        'گزینه چهارم': '',
    };
    let {question} = props;

    if (Object.keys(question).length) {
        formData = {
            id: question['id'],
            title: question['label'],
            questionText: question['description'],
            answer: null,
            comments: null,
            difficulty: (question['difficulty'] || '').replace(/\D/g, ''),
            isPublic: question['isPublic'],
            rightChoice: 1,
            choiceset: Object.values(question['options']),
            categories: question['cat'],
            tags: question['tag'],
        };
    }

    const handleFormData = (key, value) => {
        formData = {
            ...formData,
            [key]: value,
        };
        console.log(formData);
    };

    useEffect(() => {
        if (
            question['options'] &&
            Object.keys(question['options']).length > 0
        ) {
            setType('test');
        } else {
            setType('detailed');
        }
    }, []);
    console.log(formData);
    return (
        <div className={'form_wrapper'}>
            <Row gutter={[16, 16]}>
                <Col span={16}>
                    <div className={'info'}>
                        <h4 className={'text_center'}>اضافه کردن صورت سوال</h4>
                        <Editor
                            getInputValue={value =>
                                handleFormData('questionText', value)
                            }
                            defaultValue={question['description']}
                        />
                    </div>
                </Col>
                <Col span={8}>
                    <Row gutter={[16, 40]}>
                        <Col span={24}>
                            <FormInput
                                getInputValue={value =>
                                    handleFormData('title', value)
                                }
                                placeholder={'مثال : انتگرال دوگانه'}
                                label={'عنوان سوال'}
                                defaultValue={question['label']}
                            />
                        </Col>
                    </Row>
                    <Row gutter={[16, 40]}>
                        <Col span={24}>
                            <FormInputNumber
                                getInputValue={value =>
                                    handleFormData('difficulty', value)
                                }
                                placeholder={'از رنج صفر تا ده'}
                                label={'درجه سختی'}
                                min={1}
                                max={10}
                                defaultValue={(
                                    question['difficulty'] || ''
                                ).replace(/\D/g, '')}
                            />
                        </Col>
                    </Row>
                    <Row gutter={[16, 40]}>
                        <Col span={12}>
                            <label className={'form_label'}>
                                <p>تگ سوال</p>
                            </label>
                            <div className={'form_input'}>
                                <QuestionTagContainer
                                    getInputValue={value =>
                                        handleFormData('tags', value)
                                    }
                                    type={'tag'}
                                    maxTagCount={2}
                                    defaultValue={question['tag']}
                                />
                            </div>
                        </Col>
                        <Col span={12}>
                            <label className={'form_label'}>
                                <p>موضوع سوال</p>
                            </label>
                            <div className={'form_input'}>
                                <QuestionTagContainer
                                    getInputValue={value =>
                                        handleFormData('categories', value)
                                    }
                                    type={'cat'}
                                    maxTagCount={2}
                                    defaultValue={question['cat']}
                                />
                            </div>
                        </Col>
                    </Row>
                    <Row gutter={[16, 40]}>
                        <Col span={12}>
                            <RadioInput
                                defaultValue={type}
                                setOption={setOption}
                                label={'نوع سوال'}
                                options={plainOptions}
                                value={type}
                            />
                        </Col>
                        <Col span={12}>
                            <CheckBoxInput
                                options={addToPublic}
                                label={'میزان دسترسی'}
                                getInputValue={value =>
                                    handleFormData('isPublic', value)
                                }
                                defaultValue={question['isPublic']}
                            />
                        </Col>
                    </Row>
                    {type === 'test' ? (
                        <Row gutter={[16, 12]}>
                            <Col span={24}>
                                <label className={'form_label'}>
                                    <p>اضافه کردن گزینه به سوال</p>
                                </label>
                                <hr className={'normal_hr'} />
                                <EditableDiv
                                    getInputValue={value =>
                                        handleFormData('choiceset', value)
                                    }
                                    content={
                                        question['options']
                                            ? question['options']
                                            : content
                                    }
                                />
                            </Col>
                            <Col span={12} dir={'rtl'}>
                                <FormInputNumber
                                    getInputValue={value =>
                                        handleFormData('rightChoice', value)
                                    }
                                    min={1}
                                    max={4}
                                    label={'گزینه صحیح'}
                                />
                            </Col>
                        </Row>
                    ) : (
                        <Row gutter={[16, 12]}>
                            <Col span={24} dir={'rtl'}>
                                <label className={'form_label'}>
                                    <p>پاسخ صحیح سوال</p>
                                </label>
                                <hr className={'normal_hr'} />
                                <FormTextArea
                                    getInputValue={value =>
                                        handleFormData('answer', value)
                                    }
                                />
                            </Col>
                        </Row>
                    )}
                    <Button
                        type={'primary'}
                        onClick={() => props.createQuestion(formData)}
                    >
                        اضافه کردن سوال
                    </Button>
                </Col>
            </Row>
        </div>
    );
}

FormView.defaultProps = {
    question: {
        label: '',
        type: '',
        difficulty: '',
        description: '',
        tag: [],
        cat: [],
        isPublic: true,
        options: {},
    },
};
