import React from 'react';
import {Input} from 'antd';
import './FormInput.css';

const {TextArea} = Input;
export default props => (
    <>
        <label className={'form_label'}>
            <p>{props.label}</p>
        </label>
        <TextArea
            className={'form_input'}
            size={'large'}
            onChange={e => props.getInputValue(e.target.value)}
            placeholder={props.placeholder}
            allowClear
            value={props.value}
            autoSize={{minRows: 3, maxRows: 5}}
        />
    </>
);
