import React from 'react';
import {Input} from 'antd';
import './FormInput.css';

export default props => (
    <>
        <label className={'form_label'}>
            <p>{props.label}</p>
        </label>
        <Input
            className={'form_input'}
            size={'large'}
            defaultValue={props.defaultValue}
            onChange={e => props.getInputValue(e.target.value)}
            placeholder={props.placeholder}
        />
    </>
);
