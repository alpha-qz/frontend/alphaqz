import React, {Fragment} from 'react';
import {Form, Input, Button} from 'antd';
import {LockOutlined, MailOutlined, UserOutlined} from '@ant-design/icons';
import './login.css';

export default props => {
    const onFinish = values => {
        props.createUser(values);
    };

    const onFinishFailed = errorInfo => {};
    return (
        <Fragment>
            <div className={'login_header'}>
                <p>ایجاد حساب کاربری</p>
            </div>

            <div className="login-warp">
                <Form
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    size={'middle'}
                    className={'ant-form-large'}
                >
                    <Form.Item
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: (
                                    <p className={'login_hint'}>
                                        لطفا یک نام کاربری برای خود انتخاب کنید.
                                    </p>
                                ),
                            },
                        ]}
                    >
                        <Input
                            size={'large'}
                            suffix={
                                <UserOutlined className="site-form-item-icon" />
                            }
                            type="text"
                            placeholder="نام کاربری"
                        />
                    </Form.Item>

                    <Form.Item
                        name="Email"
                        rules={[
                            {
                                required: true,
                                message: (
                                    <p className={'login_hint'}>
                                        لطفا ایمیل خود را وارد نمائید.
                                    </p>
                                ),
                            },
                        ]}
                    >
                        <Input
                            size={'large'}
                            suffix={
                                <MailOutlined className="site-form-item-icon" />
                            }
                            type="email"
                            placeholder="ایمیل"
                        />
                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: (
                                    <p className={'login_hint'}>
                                        لطفا پسورد خود را وارد نمائید.
                                    </p>
                                ),
                            },
                        ]}
                    >
                        <Input
                            size={'large'}
                            suffix={
                                <LockOutlined className="site-form-item-icon" />
                            }
                            type="password"
                            placeholder="رمز عبور"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button
                            htmlType="submit"
                            className="login-form-button mt-5 create_acc"
                            block
                            size={'large'}
                            loading={props.loading}
                        >
                            ایجاد حساب کاربری
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </Fragment>
    );
};
