import React from 'react';
import {Row, Col} from 'antd';
import './login.css';
import loginImg from '../../Assets/Group 64.png';
import MovingSvgs from "../UI's/svg/MovingSvgs";

export default props => (
    <div className={'login_wrapper'}>
        <MovingSvgs />
        <div className="bg_grey" key={'div'}>
            <Row justify="center" key={'row'}>
                <Col span={14}>
                    <div className={'login_img_wrapper'}>
                        <img
                            src={loginImg}
                            alt={'An error occurred while loading'}
                        />
                    </div>
                </Col>
                <Col span={6} className={'login_form_wrapper'}>
                    {props.children}
                </Col>
            </Row>
        </div>
    </div>
);
