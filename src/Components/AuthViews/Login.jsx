import React, {Fragment} from 'react';
import {Form, Input, Button} from 'antd';
import {MailOutlined, LockOutlined} from '@ant-design/icons';
import './login.css';
import {Link} from 'react-router-dom';
import * as Routes from '../../Constants/Routes/Routes';

export default props => {
    const onFinish = values => {
        props.login(values);
    };

    const onFinishFailed = errorInfo => {};
    return (
        <Fragment>
            <div className={'login_header'}>
                <p>ورود به سامانه</p>
            </div>
            <div className="login-warp">
                <Form
                    size={'middle'}
                    name="basic"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    className={'ant-form-large'}
                >
                    <Form.Item
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: (
                                    <p className={'login_hint'}>
                                        ایمیل خود را وارد کنید
                                    </p>
                                ),
                            },
                        ]}
                    >
                        <Input
                            size={'large'}
                            suffix={
                                <MailOutlined className="site-form-item-icon" />
                            }
                            placeholder="ایمیل"
                        />
                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: (
                                    <p className={'login_hint'}>
                                        لطفا رمز عبور خود را وارد نمائید.
                                    </p>
                                ),
                            },
                        ]}
                    >
                        <Input
                            size={'large'}
                            suffix={
                                <LockOutlined className="site-form-item-icon" />
                            }
                            type="password"
                            placeholder="رمز عبور"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                            block={true}
                            loading={props.loading}
                            size={'large'}
                        >
                            ورود
                        </Button>
                    </Form.Item>
                </Form>
                <Link to={Routes.signUpLink}>
                    <Button
                        className="login-form-button mt-5 create_acc"
                        block={true}
                        size={'large'}
                    >
                        ایجاد حساب کاربری
                    </Button>
                </Link>
            </div>
        </Fragment>
    );
};
