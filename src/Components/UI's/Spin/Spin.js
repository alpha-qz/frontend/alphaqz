import React from 'react';
import {Spin} from 'antd';
import {LoadingOutlined} from '@ant-design/icons';

export default props => (
    <>
        {props.loading ? (
            <div className={'text_center'}>
                <Spin
                    indicator={<LoadingOutlined style={{fontSize: 24}} spin />}
                />
            </div>
        ) : (
            props.children
        )}
    </>
);
