import React, {useState, useEffect} from 'react';
import persianJs from 'persianjs';

const clockStyle = {
    fontFamily: 'IRANSans',
    direction: 'rtl',
};

export default function CountDownTime(props) {
    const calculateTimeLeft = () => {
        const difference = +new Date(props.date) - +new Date();
        let timeLeft = {};

        if (difference > 0) {
            timeLeft = {
                روز: Math.floor(difference / (1000 * 60 * 60 * 24)),
                ساعت: Math.floor((difference / (1000 * 60 * 60)) % 24),
                دقیقه: Math.floor((difference / 1000 / 60) % 60),
                ثانیه: Math.floor((difference / 1000) % 60),
            };
        }

        return timeLeft;
    };

    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

    useEffect(() => {
        setTimeout(() => {
            setTimeLeft(calculateTimeLeft());
        }, 1000);
    });

    const timerComponents = [];

    Object.keys(timeLeft).forEach(interval => {
        if (!timeLeft[interval]) {
            return;
        }
        timerComponents.push(
            <span key={timeLeft[interval] + String(Math.random())}>
                {String(persianJs(timeLeft[interval]).englishNumber())}{' '}
                {interval}{' '}
            </span>,
        );
    });

    return (
        <div style={clockStyle}>
            {timerComponents.length ? (
                timerComponents
            ) : (
                <span>آزمون به پایان رسیده است</span>
            )}
        </div>
    );
}
