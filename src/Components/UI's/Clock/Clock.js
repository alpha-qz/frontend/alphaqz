import React from 'react';
import persianJs from 'persianjs';

import './Clock.css';

export const Clock = props => {
    const [date, setDate] = React.useState(new Date());

    // Replaces componentDidMount and componentWillUnmount
    React.useEffect(() => {
        let timerID = setInterval(() => tick(), 1000);
        return function cleanup() {
            clearInterval(timerID);
        };
    });

    function tick() {
        setDate(new Date());
    }

    return (
        <div className={'clock_container'}>
            <h2>
                {String(persianJs(date.toLocaleTimeString()).englishNumber())}.
            </h2>
        </div>
    );
};
