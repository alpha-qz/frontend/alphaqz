import React from 'react';
import './Badge.css';

export default props => (
    <p
        className={`logo_badge ${props.type}`}
        style={{
            backgroundColor: props.bgColor,
            color: props.color,
            fontSize: props.fs,
        }}
    >
        {props.title}
    </p>
);
