import React from 'react';
import TweenOne from 'rc-tween-one';
import './Banner.css';

const loop = {
    duration: 3000,
    yoyo: true,
    repeat: -1,
};

export default props => (
    <div className="banner-bg-wrapper">
        <svg width="400px" height="576px" viewBox="0 0 400 576" fill="none">
            <TweenOne
                component="g"
                animation={[
                    {opacity: 0, type: 'from'},
                    {...loop, y: 15},
                ]}
            >
                <ellipse
                    id="Oval-9-Copy-4"
                    cx="100"
                    cy="100"
                    rx="6"
                    ry="6"
                    stroke="#2F54EB"
                    strokeWidth="1.6"
                />
            </TweenOne>
            <TweenOne
                component="g"
                animation={[
                    {opacity: 0, type: 'from'},
                    {...loop, y: 15},
                ]}
            >
                <ellipse
                    id="Oval-9-Copy-43"
                    cx="10"
                    cy="150"
                    rx="6"
                    ry="6"
                    stroke="#008274"
                    strokeWidth="1.6"
                />
            </TweenOne>
            <TweenOne
                component="g"
                animation={[
                    {opacity: 0, type: 'from'},
                    {...loop, y: 15},
                ]}
            >
                <ellipse
                    id="Oval-9-Copy-4"
                    cx="500"
                    cy="500"
                    rx="6"
                    ry="6"
                    stroke="#2F54EB"
                    strokeWidth="1.6"
                />
            </TweenOne>
            <TweenOne
                component="g"
                animation={[
                    {opacity: 0, type: 'from'},
                    {...loop, y: -15},
                ]}
            >
                <g transform="translate(100 50)">
                    <g
                        style={{
                            transformOrigin: '50% 50%',
                            transform: 'rotate(-340deg)',
                        }}
                    >
                        <rect
                            stroke="#008274"
                            strokeWidth="1.6"
                            width="9"
                            height="9"
                        />
                    </g>
                </g>
            </TweenOne>
            <TweenOne
                component="g"
                animation={[
                    {opacity: 0, type: 'from'},
                    {...loop, y: -15},
                ]}
            >
                <g transform="translate(200 400)">
                    <g
                        style={{
                            transformOrigin: '50% 50%',
                            transform: 'rotate(-340deg)',
                        }}
                    >
                        <rect
                            stroke="#FADB14"
                            strokeWidth="1.6"
                            width="9"
                            height="9"
                        />
                    </g>
                </g>
            </TweenOne>
        </svg>
    </div>
);
