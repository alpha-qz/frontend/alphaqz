import React from 'react';
import {withRouter} from 'react-router-dom';
import {Collapse} from 'antd';
import {Menu, Dropdown} from 'antd';
import PersianDigit from '../PersianDigits/PersianDigit';
import Tag from '../Tag/Tag';
import './SubmissionsView.css';

const {Panel} = Collapse;

function SubmissionView(props) {
    const callback = () => {};

    const menu = (
        <Menu>
            <Menu.Item key="1">ایجاد اعتراض</Menu.Item>
            <Menu.Item key="2" onClick={props.removeSubmission}>
                حذف ارسال
            </Menu.Item>
        </Menu>
    );

    return (
        <div>
            <Collapse
                expandIconPosition={'left'}
                defaultActiveKey={['0']}
                onChange={callback}
            >
                {props.data.map((item, idx) => (
                    <Panel header={`سوال شماره ${item.question}`} key={idx}>
                        <Dropdown overlay={menu} trigger={['contextMenu']}>
                            <div className={'submission_details_wrapper'}>
                                <div>
                                    <p>
                                        <span
                                            className={
                                                'submission_detail_label'
                                            }
                                        >
                                            جواب داده شده :
                                        </span>
                                        {item.answer}
                                    </p>

                                    <p>
                                        <span
                                            className={
                                                'submission_detail_label'
                                            }
                                        >
                                            وضعیت تصحیح :
                                        </span>
                                        <Tag bgColor={'orange'}>
                                            {item.graded
                                                ? 'تصحیح شده'
                                                : 'تصحیح نشده'}
                                        </Tag>
                                    </p>
                                </div>
                                <div>
                                    <p>
                                        <span
                                            className={
                                                'submission_detail_label'
                                            }
                                        >
                                            نمره :
                                        </span>
                                        <Tag bgColor={'orange'}>
                                            <PersianDigit>
                                                {item.grade}
                                            </PersianDigit>
                                        </Tag>
                                    </p>
                                    <p>
                                        <span
                                            className={
                                                'submission_detail_label'
                                            }
                                        >
                                            آزمون :
                                        </span>
                                        {item.quiz}
                                    </p>
                                </div>
                            </div>
                        </Dropdown>
                    </Panel>
                ))}
            </Collapse>
        </div>
    );
}
export default withRouter(SubmissionView);
