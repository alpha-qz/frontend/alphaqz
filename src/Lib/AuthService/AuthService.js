// "ID": 40,
//       "CreatedAt": "2020-03-25T06:18:22.079106+04:30",
//       "UpdatedAt": "2020-03-25T06:18:22.079106+04:30",
//       "DeletedAt": null,
//       "email": "dx@dx.dx",
//       "password": "",
//       "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjQwfQ.ykga0h2nAVoEsRXv_Y5cji_Fq5UFfSFloaschnXLGhQ"

import {confirmToken} from '../ApiService/Private';
import ResponseHandler from '../ResponseHandler';
import ErrorHandler from '../ErrorHandler';

export default class AuthService {
    tokenIsValid = () => {
        let token = sessionStorage.getItem('token');
        if (token) {
            return confirmToken()
                .then(response => {
                    return ResponseHandler(response).status;
                })
                .catch(err => {
                    return ErrorHandler(err).status;
                });
        } else {
            return false;
        }
    };

    logout = () => {
        sessionStorage.clear();
    };

    login = data => {
        sessionStorage.setItem('id', data.account.ID);
        sessionStorage.setItem('username', data.account.username);
        sessionStorage.setItem('email', data.account.email);
        sessionStorage.setItem('token', data.account.token);
    };
}
