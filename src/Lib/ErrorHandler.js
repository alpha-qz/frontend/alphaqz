export default error => {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        switch (error.response.status) {
            case 400:
                return {
                    message: 'خطا !',
                    serverMessage: error.response.data.message,
                    code: error.response.status,
                    status: error.response.data.status,
                    type: 'error',
                };
            case 401:
                return {
                    message: 'خطا دسترسی !',
                    serverMessage: error.response.data.message,
                    code: error.response.status,
                    status: error.response.data.status,
                    type: 'error',
                };
            case 403:
                return {
                    message: 'عملیات موفقیت آمیز نبود!',
                    serverMessage: error.response.data.message,
                    code: error.response.status,
                    status: error.response.data.status,
                    type: 'error',
                };
            case 404:
                return {
                    message: 'عملیات موفقیت آمیز نبود!',
                    serverMessage: error.response.data.message,
                    code: error.response.status,
                    status: error.response.data.status,
                    type: 'error',
                };
            case 500:
                return {
                    message: 'مشکلی در سرور پیش آمده است!',
                    serverMessage: error.response.data.message,
                    code: error.response.status,
                    status: error.response.data.status,
                    type: 'error',
                };
            case 502:
                return {
                    message: 'Bad Gateway‌',
                    serverMessage: error.response.data.message,
                    code: error.response.status,
                    status: error.response.data.status,
                    type: 'error',
                };
            case 503:
                return {
                    message: 'Service Unavailable‌',
                    serverMessage: error.response.data.message,
                    code: error.response.status,
                    status: error.response.data.status,
                    type: 'error',
                };
            case 504:
                return {
                    message: 'Gateway Timeout‌',
                    serverMessage: error.response.data.message,
                    code: error.response.status,
                    status: error.response.data.status,
                    type: 'error',
                };
            default:
                return {
                    message: 'Gateway Timeout‌',
                    type: 'error',
                };
        }
    } else if (error.request) {
        return {
            message: 'خطا هنگام اتصال به شبکه',
            serverMessage: 'Network Error',
            code: 0,
            status: false,
            type: 'error',
        };
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        // console.log(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        return {
            message: error.message,
            code: 0,
            status: false,
            type: 'error',
        };
    }
};
