export default response => {
    switch (response.status) {
        case 200:
            return {
                message: 'عملیات با موفقیت انجام شد',
                serverMessage: response.data.message,
                status: response.data.status,
                type: 'success',
            };
        case 201:
            return {
                message: 'تغییرات با موفقیت ایجاد شد',
                serverMessage: response.data.message,
                status: response.data.status,
                type: 'success',
            };
        case 202:
            return {
                message: 'تغییرات با موفقیت ایجاد شد',
                serverMessage: response.data.message,
                status: response.data.status,
                type: 'success',
            };
        case 204:
            return {
                message: 'تغییرات با موفقیت ایجاد شد',
                serverMessage: response.data.message,
                status: response.data.status,
                type: 'success',
            };
        default:
            return {
                message: 'تغییرات با موفقیت ایجاد شد',
                type: 'success',
            };
    }
};
