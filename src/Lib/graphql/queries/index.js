import {gql} from '@apollo/client';

export const GET_ALL_TAGS = gql`
    {
        allTags {
            edges {
                node {
                    title
                }
            }
        }
    }
`;

export const GET_ALL_CATS = gql`
    {
        allCategories {
            edges {
                node {
                    id
                    title
                }
            }
        }
    }
`;

export const GET_ALL_QUESTION = gql`
    {
        allQuestions(isPublic: true) {
            edges {
                node {
                    id
                    title
                    difficulty
                    isPublic: isPublic
                    creatorName
                    choiceset {
                        id
                        choice1
                        choice2
                        choice3
                        choice4
                    }
                    qtag {
                        edges {
                            node {
                                tag {
                                    title
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;

export const GET_QUESTION_BY_ID = gql`
    query getQuestion($id: ID!) {
        question(id: $id) {
            id
            title
            questionText
            choiceset {
                id
                choice1
                choice2
                choice3
                choice4
            }
            creatorName
            comments
            difficulty
            isPublic
            qcategory {
                edges {
                    node {
                        category {
                            title
                        }
                    }
                }
            }
            qtag {
                edges {
                    node {
                        tag {
                            title
                        }
                    }
                }
            }
        }
    }
`;

export const CREATE_QUESTION = gql`
    mutation createQuestion(
        $title: String!
        $questionText: String!
        $answer: String
        $comments: String
        $difficulty: Int!
        $isPublic: Boolean!
        $choiceset: [String!]
        $categories: [String!]
        $tags: [String!]
        $rightChoice: Int!
        $token: String!
    ) {
        createQuestion(
            title: $title
            questionText: $questionText
            answer: $answer
            comments: $comments
            difficulty: $difficulty
            isPublic: $isPublic
            rightChoice: $rightChoice
            choiceset: $choiceset
            categories: $categories
            tags: $tags
            token: $token
        ) {
            question {
                id
            }
        }
    }
`;

export const EDIT_QUESTION = gql`
    mutation editQuestion(
        $id: Int!
        $title: String!
        $questionText: String!
        $answer: String
        $comments: String
        $difficulty: Int!
        $isPublic: Boolean!
        $choiceset: [String!]
        $categories: [String!]
        $tags: [String!]
        $rightChoice: Int!
        $token: String!
    ) {
        createQuestion(
            id: $id
            title: $title
            questionText: $questionText
            answer: $answer
            comments: $comments
            difficulty: $difficulty
            isPublic: $isPublic
            rightChoice: $rightChoice
            choiceset: $choiceset
            categories: $categories
            tags: $tags
            token: $token
        ) {
            question {
                title
                qtag {
                    edges {
                        node {
                            tag {
                                title
                            }
                        }
                    }
                }
            }
        }
    }
`;

export const FILTER_QUESTIONS = gql`
    query FilterQuestions(
        $title: String!
        $cat: String!
        $tag: String!
        $questionText: String!
    ) {
        allQuestions(
            title_Icontains: $title
            questionText_Icontains: $questionText
            qtag_Tag_Title_Icontains: $tag
            qcategory_Category_Title_Icontains: $cat
        ) {
            edges {
                node {
                    id
                    title
                    difficulty
                    isPublic
                    creatorName
                    choiceset {
                        id
                        choice1
                        choice2
                        choice3
                        choice4
                    }
                    qtag {
                        edges {
                            node {
                                tag {
                                    title
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;

export const GET_MY_QUESTIONS = gql`
    query GetMyQuestion($token: String!) {
        myQuestions(token: $token) {
            edges {
                node {
                    id
                    title
                    difficulty
                    isPublic
                    creatorName
                    choiceset {
                        id
                        choice1
                        choice2
                        choice3
                        choice4
                    }
                    qcategory {
                        edges {
                            node {
                                category {
                                    title
                                }
                            }
                        }
                    }
                    qtag {
                        edges {
                            node {
                                tag {
                                    title
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;

export const GET_QUESTIONS_BY_IDS = gql`
    query getQuestionBIds($ids: [Int]) {
        allQuestions(ids: $ids) {
            edges {
                node {
                    id
                    title
                    difficulty
                    isPublic
                    creatorName
                    questionText
                    choiceset {
                        id
                        choice1
                        choice2
                        choice3
                        choice4
                    }
                    qcategory {
                        edges {
                            node {
                                category {
                                    title
                                }
                            }
                        }
                    }
                    qtag {
                        edges {
                            node {
                                tag {
                                    title
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;
