import persianJs from 'persianjs';
export const getPersianNumber = int => String(persianJs(int).englishNumber());
