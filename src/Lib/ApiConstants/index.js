// user account endpoints
export const baseUrl = 'http://194.5.192.210:8080';
export const users = '/users';
export const register = '/register';
export const me = '/me';
export const login = '/login';
export const logout = '/logout';

export const examBaseUrl = 'http://194.5.192.210:8570';
export const exams = '/quizzes/';
export const exam = examId => `${exams}${examId}/`;
export const examQuestions = examId => `${exam(examId)}questions/`;
export const deleteQuestion = (examId, qId) =>
    `${examQuestions(examId)}${qId}/`;

export const submissions = () => '/submissions/';
export const submission = submissionsId => `${submissions()}${submissionsId}/`;
export const getExamSubmissions = examId => `${exams}${examId}${submissions()}`;
