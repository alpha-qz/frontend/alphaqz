import axios from 'axios';
import * as endPoint from '../../ApiConstants/index';

export const getToken = () => {
    let token = sessionStorage.getItem('token');
    return `Bearer ${token}`;
};

export const Api = axios.create({
    baseURL: endPoint.baseUrl,
    timeout: 3000,
    headers: {
        'Content-Type': 'application/json',
        Authorization: getToken(),
    },
});

export const examApi = axios.create({
    baseURL: endPoint.examBaseUrl,

    timeout: 3000,
    headers: {
        'Content-Type': 'application/json',
        Authorization: getToken(),
    },
});

// User api functions
export const confirmToken = () =>
    Api.get(endPoint.users + endPoint.me, {
        headers: {
            Authorization: getToken(),
        },
    });

export const logoutAccount = () =>
    Api.post(endPoint.users + endPoint.logout, {
        headers: {
            Authorization: getToken(),
        },
    });

// Exam api functions
export const createExam = data => examApi.post(endPoint.exams, data);

export const getAllExams = () => examApi.get(endPoint.exams);

export const getExam = examId => examApi.get(endPoint.exam(examId));

export const editExam = (examId, data) =>
    examApi.put(endPoint.exam(examId), data);

export const getExamQuestions = examId =>
    examApi.get(endPoint.examQuestions(examId));

export const removeExam = examId => examApi.delete(endPoint.exam(examId));

export const addQuestions = (examId, data) =>
    examApi.post(endPoint.examQuestions(examId), data);

export const getUsersExams = () => examApi.get(endPoint.exams);

export const deleteQuestionFromExam = (qId, examId) =>
    examApi.delete(endPoint.deleteQuestion(examId, qId));

// Submissions api function
export const getAllSubmissions = () => examApi.get(endPoint.submissions());

export const makeSubmission = data =>
    examApi.post(endPoint.submissions(), data);

export const getSubmission = submissionId =>
    examApi.get(endPoint.submission(submissionId));

export const editSubmission = (submissionId, data) =>
    examApi.put(endPoint.submission(submissionId), data);

export const deleteSubmission = submissionId =>
    examApi.delete(endPoint.submission(submissionId));

export const getExamSubmissions = examId =>
    examApi.get(endPoint.getExamSubmissions(examId));
