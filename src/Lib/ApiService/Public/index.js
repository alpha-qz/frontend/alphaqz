import axios from 'axios';
import * as endPoint from '../../ApiConstants/index';

export const Api = axios.create({
    baseURL: endPoint.baseUrl,
    timeout: 3000,
    headers: {
        'Content-Type': 'application/json',
    },
});

export const createUser = data =>
    Api.post(endPoint.users + endPoint.register, data);

export const login = data => Api.post(endPoint.users + endPoint.login, data);
