import React from 'react';
import Sidebar from '../Components/Layout/Sidebar/Sidebar';
import LayoutView from '../Components/Layout/LayoutView';
import {Layout} from 'antd';
import MainFooter from './MainFooter';
import {Switch, Route, withRouter} from 'react-router-dom';
import * as Routes from '../Constants/Routes/Routes';
import ExamsContainer from './Exam/ExamsContainer';
import CreateExamContainer from './Exam/CreateExamContainer';
import QuestionsContainer from './Question/QuestionsContainer';
import CreateQuestion from './Question/CreateQuestion';
import QuestionContainer from './Question/QuestionContainer';
import MyExams from './Exam/MyExams';
import MyQuestions from './Question/MyQuestions';
import {
    ProfileOutlined,
    BarChartOutlined,
    UploadOutlined,
    QuestionCircleOutlined,
} from '@ant-design/icons';
import SubmissionsContainer from './Submission/SubmissionsContainer';

const itemStyles = {
    display: 'flex',
    height: '50px',
};

function LayoutContainer(props) {
    const findMatchingKey = () => {
        const urls = ['exam', '/test1', '/test2', '/test3'];
        const urlsArray = props.location.pathname.split('/');
        const url = urlsArray[urlsArray.length - 1];
        switch (url) {
            case urls[0]:
                return '1';
            case urls[1]:
                return '2';
            case urls[2]:
                return '3';
            case urls[3]:
                return '4';
            default:
                return '0';
        }
    };

    const classId = props.match.params.class_id;
    const linksSpecifications = [
        {
            label: 'امتحانات',
            path: Routes.examsLink(classId),
            key: '1',
            icon: <ProfileOutlined />,
        },
        {
            label: 'سوالات',
            path: Routes.questionsLink(classId),
            key: '2',
            icon: <QuestionCircleOutlined />,
        },
        {
            label: 'ارسالات',
            path: Routes.mySubmissionsLink(classId),
            key: '3',
            icon: <UploadOutlined />,
        },
        {
            label: 'Nav 1',
            path: '/test3',
            key: '4',
            icon: <BarChartOutlined />,
        },
    ];

    return (
        <Layout>
            <Sidebar
                links={linksSpecifications}
                itemStyles={itemStyles}
                defaultSelectedKey={findMatchingKey()}
            />
            <LayoutView
                contentStyle={{paddingBottom: '70px'}}
                footer={<MainFooter />}
            >
                <Switch>
                    <Route
                        exact
                        path={Routes.examsRoute}
                        component={ExamsContainer}
                    />
                    <Route
                        exact
                        path={[Routes.createExamRoute, Routes.editExamRoute]}
                        component={CreateExamContainer}
                    />
                    <Route
                        exact
                        path={Routes.questionsRoute}
                        component={QuestionsContainer}
                    />
                    <Route
                        exact
                        path={[
                            Routes.createQuestionRoute,
                            Routes.editQuestionRoute,
                        ]}
                        component={CreateQuestion}
                    />
                    <Route
                        exact
                        path={Routes.myQuestionsRoute}
                        component={MyQuestions}
                    />
                    <Route
                        exact
                        path={Routes.aQuestionRoute}
                        component={QuestionContainer}
                    />
                    <Route path={Routes.myExamsRoute} component={MyExams} />
                    <Route
                        exact
                        path={[
                            Routes.mySubmissionRoute,
                            Routes.mySubmissionsRoute,
                        ]}
                        component={SubmissionsContainer}
                    />
                </Switch>
            </LayoutView>
        </Layout>
    );
}

export default withRouter(LayoutContainer);
