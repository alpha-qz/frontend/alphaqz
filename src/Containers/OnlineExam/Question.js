import React, {useState, useEffect} from 'react';
import {withRouter} from 'react-router-dom';
import {notification} from 'antd';
import QuestionHeader from '../../Components/Question/QuestionHeader';
import TestQuestion from '../../Components/Question/TestQuestion';
import DetailedQuestion from '../../Components/Question/DetailedQuestion';
import * as ApiService from '../../Lib/ApiService/Private/index';
import ErrorHandler from '../../Lib/ErrorHandler';
import LoadingOverlay from '../../Components/LoadingOverlay/LoadingOverlay';
import ResponseHandler from '../../Lib/ResponseHandler';
function Question(props) {
    const [submissionLoading, setSubmissionLoading] = useState(false);
    const [requestMethod, setRequestMethod] = useState('post');
    const examId = parseInt(props.match.params.exam_id);
    const {question, OnlineTest, submission} = props;
    const makeSubmission = answer => {
        if (!OnlineTest) return;
        setSubmissionLoading(true);
        let questionId = question['question'];
        let data = {
            question: questionId,
            quiz: examId,
            answer: answer,
        };
        if (requestMethod === 'post') {
            postSubmission(data);
        } else {
            putSubmission(submission['id'], data);
        }
    };

    useEffect(() => {
        checkRequestMethod();
    }, []);

    const checkRequestMethod = () => {
        if (!OnlineTest) return;
        if (Object.keys(submission).length > 0) {
            setRequestMethod('put');
        }
    };

    const postSubmission = data =>
        ApiService.makeSubmission(data)
            .then(res => {
                setSubmissionLoading(false);
                res = ResponseHandler(res);
                notification[res.type]({
                    message: <p dir={'rtl'}>پاسخ با موفقیت ثبت شد</p>,
                    duration: 3,
                });
            })
            .catch(err => {
                setSubmissionLoading(false);
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });

    const putSubmission = (submissionId, data) =>
        ApiService.editSubmission(submissionId, data)
            .then(res => {
                setSubmissionLoading(false);
                res = ResponseHandler(res);
                notification[res.type]({
                    message: <p dir={'rtl'}>پاسخ با موفقیت تغییر یافت.</p>,
                    duration: 3,
                });
            })
            .catch(err => {
                setSubmissionLoading(false);
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });

    return (
        <>
            <QuestionHeader
                OnlineTest={props.OnlineTest}
                qAuthor={props.question['creatorName']}
                qLabel={props.question['label']}
                cLabel={props.question['course']}
            />
            {props.question['type'] === 'test' ? (
                <LoadingOverlay
                    active={submissionLoading}
                    unmount={false}
                    spinner
                    fadeSpeed={100}
                    styles={{backgroundColor: 'white'}}
                >
                    <TestQuestion
                        {...props}
                        defualtAnswer={submission['answer']}
                        onChange={makeSubmission}
                    />
                </LoadingOverlay>
            ) : (
                <LoadingOverlay
                    active={submissionLoading}
                    spinner
                    fadeSpeed={100}
                    styles={{backgroundColor: 'white'}}
                >
                    <DetailedQuestion {...props} onChange={makeSubmission} />
                </LoadingOverlay>
            )}
        </>
    );
}
Question.defaultProps = {
    submission: {},
};
export default withRouter(Question);
