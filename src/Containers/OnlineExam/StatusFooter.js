import React from 'react';
import {Button} from 'antd';
import Footer from '../../Components/Footer/Footer';
import persianJs from 'persianjs';

const obj = {
    answeredQ: [4, 'سوالات جواب داده شده'],
    notAnsweredQ: [5, 'سوالات جواب داده نشده'],
    doubtfulQ: [5, 'شک دار'],
    allQ: [9, 'پاسخ داده شده'],
};

export default function StatusFooter(props) {
    return (
        <Footer>
            {Object.keys(obj).map(item => (
                <h5 key={item[1] + item[0]}>{`${obj[item][1]} : ${persianJs(
                    obj[item][0],
                ).englishNumber()}`}</h5>
            ))}
            <Button type={'primary'}>اتمام آزمون</Button>
        </Footer>
    );
}
