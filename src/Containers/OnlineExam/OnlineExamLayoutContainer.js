import React, {useState, useEffect} from 'react';
import Sidebar from '../../Components/Layout/Sidebar/Sidebar';
import LayoutView from '../../Components/Layout/LayoutView';
import {Layout, notification} from 'antd';
import StatusFooter from './StatusFooter';
import * as Routes from '../../Constants/Routes/Routes';
import {withRouter, Switch, Route} from 'react-router-dom';
import Question from './Question';
import LoadingOverlay from '../../Components/LoadingOverlay/LoadingOverlay';
import * as ApiService from '../../Lib/ApiService/Private/index';
import ErrorHandler from '../../Lib/ErrorHandler';
import {getPersianNumber} from '../../Lib/Utility/persianNumber';
import Hint from '../../Components/Hint/AddQuestionHint';
import {useQuery} from '@apollo/react-hooks';
import {GET_QUESTIONS_BY_IDS} from '../../Lib/graphql/queries';
import PersianDigit from '../../Components/PersianDigits/PersianDigit';
const itemStyles = {
    display: 'flex',
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    height: '45px',
};

const pointStyle = {
    display: 'flex',
    alignItems: 'center',
    padding: '0px 10px',
    height: '30px',
    backgroundColor: '#05A6F0',
    borderRadius: '4px',
    marginLeft: '10px',
    color: 'white',
};

const questionSpecifications = [
    {
        label: 'سوال ۱',
        course: 'سیگنال ها و سیستم ها',
        path: Routes.examQuestionLink(3452, 124, 1),
        type: 'test',
        key: '1',
        point: 10,
        icon: <div style={pointStyle}>۱۰</div>,
        description:
            'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.',
        options: {
            1: 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است',
            2: 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه ',
            3: 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است',
            4: 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه ',
        },
    },
];

function OnlineExamLayoutContainer(props) {
    const examId = parseInt(props.match.params.exam_id);
    const classId = parseInt(props.match.params.class_id);
    const [submissions, setSubmissions] = useState({});
    const [getQuestionLoading, setGetQuestionLoading] = useState(true);
    const [questionsList, setQuestionsList] = useState([]);
    const {refetch: getQuestionsByIds} = useQuery(GET_QUESTIONS_BY_IDS);
    useEffect(() => {
        fetchAllData();
    }, []);

    const fetchAllData = async () => {
        await getExamQuestions(examId);
        await getExamSubmissionIfExist(examId);
        setGetQuestionLoading(false);
    };

    const getQuestionsDetail = async ids => {
        let idsArray = ids.map(item => item['question']);
        if (idsArray.length === 0) {
            return;
        }
        const {data} = await getQuestionsByIds({
            variables: {ids: idsArray},
        });
        console.log(ids);
        console.log(data);
        let questionsDetails = data.allQuestions.edges.map((item, idx) => {
            delete item['node']['choiceset']['id'];
            delete item['node']['choiceset']['__typename'];
            return {
                question: ids[idx]['question'],
                label: `سوال ${getPersianNumber(idx + 1)}`,
                point: ids[idx]['points'],
                key: idx,
                icon: (
                    <div style={pointStyle}>
                        <PersianDigit>
                            {String(ids[idx]['points'])}
                        </PersianDigit>
                    </div>
                ),
                path: Routes.examQuestionLink(classId, examId, idsArray[idx]),
                description: item['node']['questionText'],
                course: item['node']['title'],
                type: 'test',
                options: item['node']['choiceset'],
            };
        });

        setQuestionsList(questionsDetails);
    };
    const getExamQuestions = async examId => {
        try {
            const _response = await ApiService.getExamQuestions(examId);
            await getQuestionsDetail(_response.data);
        } catch (err) {
            let res = ErrorHandler(err);
            notification[res.type]({
                message: <p dir={'rtl'}>{res.message}</p>,
                description: <p dir={'rtl'}>{res.serverMessage}</p>,
                duration: 3,
            });
        }
    };

    const findMatchingKey = () => {
        if (!getQuestionLoading) {
            const ids = questionsList.map(item => item['question']);
            const urlsArray = props.location.pathname.split('/');
            const url = urlsArray[urlsArray.length - 1];
            return String(ids.lastIndexOf(parseInt(url)));
        }
    };

    const getQuestion = (qId, index = false) => {
        if (index) return questionsList[qId];
        let questionIndex = questionsList.findIndex(
            item => item['question'] == qId,
        );
        return questionsList[questionIndex];
    };

    const getSubmission = (qId, index = false) => {
        let questionId;
        if (index) {
            questionId = questionsList[qId]['question'];
        } else {
            questionId = qId;
        }
        return submissions[questionId];
    };

    const changeSubmissionsShape = submissions => {
        let newSubmissions = {};
        submissions.map(item => {
            return (newSubmissions[item['question']] = item);
        });
        setSubmissions(newSubmissions);
    };

    const getExamSubmissionIfExist = examId => {
        return ApiService.getExamSubmissions(examId)
            .then(res => {
                changeSubmissionsShape(res.data.results);
            })
            .catch(err => console.log(err));
    };

    const getQuestionComponent = props => {
        let questionId = parseInt(props.match.params.question_id) || 0;
        return (
            <Question
                OnlineTest={true}
                {...props}
                submission={getSubmission(questionId, !questionId)}
                question={getQuestion(questionId, !questionId)}
            />
        );
    };

    return (
        <Layout>
            <Sidebar
                links={questionsList}
                itemStyles={itemStyles}
                defaultSelectedKey={findMatchingKey()}
            />

            <LayoutView
                contentStyle={{paddingBottom: '30px'}}
                footer={<StatusFooter />}
            >
                <LoadingOverlay
                    active={getQuestionLoading}
                    spinner
                    fadeSpeed={100}
                    styles={{backgroundColor: 'white'}}
                >
                    {questionsList.length === 0 ? (
                        <Hint />
                    ) : (
                        <Switch>
                            <Route
                                exact
                                path={Routes.examQuestionRoute}
                                component={props => getQuestionComponent(props)}
                            />
                            <Route
                                path={Routes.examRoute}
                                component={props => getQuestionComponent(props)}
                            />
                        </Switch>
                    )}
                </LoadingOverlay>
            </LayoutView>
        </Layout>
    );
}

export default withRouter(OnlineExamLayoutContainer);
