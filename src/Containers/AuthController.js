import React, {Component} from 'react';
import {Switch, withRouter, Route} from 'react-router-dom';
import {notification} from 'antd';
import AuthView from '../Components/AuthViews/AuthView';
import Login from '../Components/AuthViews/Login';
import Signup from '../Components/AuthViews/Signup';
import {createUser, login} from '../Lib/ApiService/Public';
import ErrorHandler from '../Lib/ErrorHandler';
import ResponseHandler from '../Lib/ResponseHandler';
import AuthService from '../Lib/AuthService/AuthService';
import LoadingOverlay from 'react-loading-overlay';
import * as Routes from '../Constants/Routes/Routes';

class AuthController extends Component {
    constructor(props) {
        super(props);
        this.auth = new AuthService();
        this.state = {
            loading: false,
            checkAuthStatus: true,
        };
    }

    componentDidMount() {
        this.onRouteChanged();
    }

    onRouteChanged = async () => {
        let status = await this.auth.tokenIsValid();
        if (status) {
            return this.redirectToTarget(Routes.classesLink);
        }
        setTimeout(
            () =>
                this.setState({
                    checkAuthStatus: false,
                }),
            500,
        );
    };

    redirectToTarget = target => {
        this.props.history.push(target);
        window.location.reload();
    };

    handleCreateUser = data => {
        this.setState({loading: true});
        createUser(data)
            .then(async response => {
                let res = ResponseHandler(response);
                notification[res.type]({
                    message: 'حساب شما با موفقیت ساخته شد.',
                    description: res.serverMessage,
                    duration: 3,
                });
                await this.auth.login(response.data);
                this.redirectToTarget(Routes.classesLink);
            })
            .catch(err => {
                this.setState({loading: false});
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
    };

    handleUserLogin = data => {
        this.setState({loading: true});
        login(data)
            .then(async response => {
                let res = ResponseHandler(response);
                notification.success({
                    message: 'شما با موفقیت وارد شدید.',
                    description: res.serverMessage,
                    duration: 3,
                });
                await this.auth.login(response.data);
                this.redirectToTarget(Routes.classesLink);
            })
            .catch(err => {
                notification.error({
                    message: <p dir={'rtl'}>{ErrorHandler(err).message}</p>,
                    description: (
                        <p dir={'rtl'}>{ErrorHandler(err).serverMessage}</p>
                    ),
                    duration: 3,
                });
                this.setState({loading: false});
            });
    };

    render() {
        return (
            <LoadingOverlay
                active={this.state.checkAuthStatus}
                spinner
                fadeSpeed={100}
                text="در حال دریافت اطلاعات"
                styles={{backgroundColor: 'white'}}
            >
                <Switch>
                    <Route
                        exact
                        path={Routes.authRoute}
                        render={() => (
                            <AuthView>
                                <Login
                                    login={this.handleUserLogin}
                                    loading={this.state.loading}
                                />
                            </AuthView>
                        )}
                    />
                    <Route
                        exact
                        path={Routes.signUpRoute}
                        render={() => (
                            <AuthView>
                                <Signup
                                    createUser={this.handleCreateUser}
                                    loading={this.state.loading}
                                />
                            </AuthView>
                        )}
                    />
                </Switch>
            </LoadingOverlay>
        );
    }
}

export default withRouter(AuthController);
