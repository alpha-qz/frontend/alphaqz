import React, {useState, useEffect} from 'react';
import * as ApiService from '../../Lib/ApiService/Private/index';
import ErrorHandler from '../../Lib/ErrorHandler';
import ResponseHandler from '../../Lib/ResponseHandler';
import Spinner from "../../Components/UI's/Spin/Spin";
import PersianDigit from '../../Components/PersianDigits/PersianDigit';
import SubmissionView from '../../Components/Submission/SubmissionView';
import {notification} from 'antd';

const data = [
    {
        question: 1,
        answer: 'string',
        graded: true,
        grade: 26,
        quiz: 'سیگنال ها و سیستم ها',
    },
    {
        question: 2,
        answer: 'string',
        graded: true,
        grade: 75,
        quiz: 'سیگنال ها و سیستم ها',
    },
    {
        question: 3,
        answer: 'string',
        graded: true,
        grade: 94,
        quiz: 'سیگنال ها و سیستم ها',
    },
];

export default function SubmissionContainer(props) {
    const [submissionLoading, setSubmissionLoading] = useState(true);
    const [removeLoading, setRemoveLoading] = useState(false);
    const [submission, setSubmission] = useState({});

    useEffect(() => {
        getSubmission(props.submissionId);
    }, []);

    const getSubmission = submissionId => {
        ApiService.getSubmission(submissionId)
            .then(async response => {
                setSubmission(response.data);
                setSubmissionLoading(false);
            })
            .catch(err => {
                err = ErrorHandler(err);
                setSubmissionLoading(false);
            });
    };

    const removeSubmission = submissionId => {
        setRemoveLoading(true);
        ApiService.deleteSubmission(submissionId)
            .then(async response => {
                let res = ResponseHandler(response);
                notification[res.type]({
                    message: 'ارسال با موفقیت حذف شد.',
                    description: res.serverMessage,
                    duration: 3,
                });
                setRemoveLoading(false);
            })
            .catch(err => {
                err = ErrorHandler(err);
                notification[err.type]({
                    message: <p dir={'rtl'}>{err.message}</p>,
                    description: <p dir={'rtl'}>{err.serverMessage}</p>,
                    duration: 3,
                });
                setRemoveLoading(false);
            });
    };

    return (
        <Spinner loading={submissionLoading || removeLoading}>
            <h3>
                ارسال شماره
                <span className={'mr-2'}>
                    <PersianDigit>{props.submissionId}</PersianDigit>
                </span>
            </h3>
            <hr className={'normal_hr_grey'} />
            <SubmissionView removeSubmission={removeSubmission} data={data} />
        </Spinner>
    );
}
