import React, {useEffect, useState} from 'react';
import {withRouter, Link} from 'react-router-dom';
import ErrorHandler from '../../Lib/ErrorHandler';
import ResponseHandler from '../../Lib/ResponseHandler';
import * as ApiService from '../../Lib/ApiService/Private/index';
import LoadingOverlay from '../../Components/LoadingOverlay/LoadingOverlay';
import {notification, Button, Modal} from 'antd';
import {ExclamationCircleOutlined} from '@ant-design/icons';
import * as Routes from '../../Constants/Routes/Routes';
import TableView from '../../Components/Table/TableView';
import Tag from '../../Components/Tag/Tag';
import PersianDigit from '../../Components/PersianDigits/PersianDigit';
import CustomSelect from '../../Components/Select/Select';
import CustomModal from '../../Components/Modal/CustomModal';
import SubmissionContainer from './SubmissionContainer';
const {confirm} = Modal;
const columns = (classId, showConfirm, handleDetailsModal, setSubmissionId) => [
    {
        title: 'شماره',
        dataIndex: 'id',
        key: 'id',
        render: (text, record) => (
            <div>
                <p>
                    <Button type={'link'}>
                        <PersianDigit>{text}</PersianDigit>
                    </Button>
                </p>
            </div>
        ),
    },
    {
        title: 'ارسال کننده',
        dataIndex: 'submitter',
        key: 'submitter',
        render: text => (
            <div className={'text_center'}>
                <p>{text}</p>
            </div>
        ),
    },
    {
        title: 'وضعیت تصحیح',
        dataIndex: 'graded',
        key: 'graded',
        render: text => (
            <div className={'text_center'}>
                <Tag type={'success'}>
                    {text ? 'تصحیح شده' : 'در حال تصحیح'}
                </Tag>
            </div>
        ),
    },
    {
        title: 'نمره',
        dataIndex: 'grade',
        key: 'grade',
        render: text => (
            <div className={'time_wrapper text_center'}>
                <Tag type={'warning'}>
                    <PersianDigit>{text}</PersianDigit>
                </Tag>
            </div>
        ),
    },
    {
        title: 'امتحان',
        dataIndex: 'quiz',
        key: 'quiz',
        render: (text, record) => (
            <Link to={Routes.editExamLink(classId, record.key)}>{text}</Link>
        ),
    },
    {
        title: '',
        dataIndex: 'delete',
        key: 'delete',
        render: (action, record) => (
            <Button
                className={'table_button'}
                size={'small'}
                onClick={() => showConfirm(record.id)}
            >
                {action}
            </Button>
        ),
    },
    {
        title: '',
        dataIndex: 'details',
        key: 'details',
        render: (action, record) => (
            <Button
                className={'table_button'}
                size={'small'}
                onClick={() => {
                    setSubmissionId(record.id);
                    handleDetailsModal();
                }}
            >
                {action}
            </Button>
        ),
    },
];
const data = [
    {
        id: 1,
        submitter: 'محمد رضا باقری',
        graded: false,
        grade: 'وارد نشده',
        quiz: '2',
        delete: 'حذف ارسال',
        details: 'جزئیات',
        key: 1,
    },
    {
        id: 2,
        submitter: 'محمد رضا باقری',
        graded: true,
        grade: '26',
        quiz: '2',
        delete: 'حذف ارسال',
        details: 'جزئیات',
        key: 1,
    },
];

function SubmissionsContainer(props) {
    const [submissionLoading, setLoading] = useState(true);
    const [examLoading, setExamLoading] = useState(true);
    const [filterMode, setFilterMode] = useState(null);
    const [tableData, setTableData] = useState(data);
    const [usersExam, setUsersExam] = useState([]);
    const [detailsModal, toggleDetailsModal] = useState(false);
    const [submissionId, setSubmissionId] = useState(null);
    const [submissions, setSubmissions] = useState([]);
    const examId = props.match.params.exam_id;
    const classId = props.match.params.class_id;

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        let exams = await getUsersExam();
        await getAllSubmissions(exams);
        // if (examId) {
        //     getExamSubmissions(examId);
        // } else {
        //     await getAllSubmissions();
        // }
    };

    const getExamSubmissions = examId => {
        setLoading(true);
        ApiService.getExamSubmissions(examId)
            .then(async response => {
                parseSubmissionsData(response.data.results, usersExam);
                setLoading(false);
            })
            .catch(err => {
                err = ErrorHandler(err);
                setLoading(false);
            });
    };

    const getUsersExam = async () => {
        setExamLoading(true);
        try {
            const response = await ApiService.getAllExams();
            setUsersExam(response.data.results);
            setExamLoading(false);
            return response.data.results;
        } catch (err) {
            err = ErrorHandler(err);
            setExamLoading(false);
        }
    };

    const parseSubmissionsData = (results, exams) => {
        console.log(exams);
        let newResults = results.map(submission => {
            let quiz = exams.filter(
                exam => exam['id'] === submission['quiz'],
            )[0];
            return {
                id: submission['id'],
                submitter: submission['submitter'],
                graded: submission['graded'],
                grade: submission['graded'] || 'وارد نشده',
                quiz: quiz['name'],
                delete: 'حذف ارسال',
                details: 'جزئیات',
                key: submission['id'],
            };
        });
        setTableData(newResults);
    };

    const getAllSubmissions = async exams => {
        setLoading(true);
        try {
            const response = await ApiService.getAllSubmissions();
            parseSubmissionsData(response.data.results, exams);
            setLoading(false);
        } catch (err) {
            err = ErrorHandler(err);
            setLoading(false);
        }
    };

    const removeSubmission = submissionId => {
        ApiService.deleteSubmission(submissionId)
            .then(async response => {
                let res = ResponseHandler(response);
                notification[res.type]({
                    message: 'ارسال با موفقیت حذف شد.',
                    description: res.serverMessage,
                    duration: 3,
                });
                window.location.reload();
            })
            .catch(err => {
                err = ErrorHandler(err);
                console.log(err);
                notification[err.type]({
                    message: <p dir={'rtl'}>{err.message}</p>,
                    description: <p dir={'rtl'}>{err.serverMessage}</p>,
                    duration: 3,
                });
            });
    };

    const handleFilter = examId => {
        if (examId === null) return;
        if (!filterMode) {
            setFilterMode(!filterMode);
            return getExamSubmissions(examId);
        } else {
            setFilterMode(!filterMode);
            return getAllSubmissions(usersExam);
        }
    };

    const handleDetailsModal = () => {
        toggleDetailsModal(!detailsModal);
    };

    const showConfirm = submissionId => {
        confirm({
            title: 'آیا مایل به حذف این ارسال هستید؟',
            icon: <ExclamationCircleOutlined />,
            okText: 'بله',
            cancelText: 'انصراف',
            onOk() {
                return removeSubmission(submissionId);
            },
            onCancel() {},
        });
    };
    return (
        <>
            <CustomSelect
                loading={examLoading}
                data={usersExam}
                defaultValue={filterMode ? examId : ''}
                btnText={filterMode ? 'حذف فیلتر' : 'فیلتر'}
                onClick={handleFilter}
            />
            <TableView
                columns={columns(
                    classId,
                    showConfirm,
                    handleDetailsModal,
                    setSubmissionId,
                )}
                pagination={false}
                dataSource={tableData}
                loading={submissionLoading}
            />
            <CustomModal
                visible={detailsModal}
                onClose={handleDetailsModal}
                cancelText={<p>بستن پنجره</p>}
                footer={[
                    <Button key="back" onClick={handleDetailsModal}>
                        بستن پنجره
                    </Button>,
                ]}
            >
                <SubmissionContainer submissionId={submissionId} />
            </CustomModal>
        </>
    );
}

export default withRouter(SubmissionsContainer);
