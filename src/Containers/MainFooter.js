import React from 'react';
import Footer from '../Components/Footer/Footer';
import Badge from "../Components/UI's/Badge/Badge";
import persianJs from 'persianjs';

const obj = {
    passedExam: [4, 'تعداد آزمون های داده شده'],
    comingExam: [5, 'تعداد آزمون های پیش رو'],
    allExam: [9, 'تعداد کل آزمون ها'],
};

export default function MainFooter(props) {
    return (
        <Footer>
            {Object.keys(obj).map((item, idx) => (
                <Badge
                    key={item + idx}
                    fs={'12px'}
                    type={'logo_outline_badge'}
                    title={`${obj[item][1]} : ${persianJs(
                        obj[item][0],
                    ).englishNumber()}`}
                />
            ))}
        </Footer>
    );
}
