import React from 'react';
import Sidebar from '../../Components/Layout/Sidebar/Sidebar';
import LayoutView from '../../Components/Layout/LayoutView';
import {Layout} from 'antd';
import ClassesContainer from './ClassesContainer';
import * as Routes from '../../Constants/Routes/Routes';
import {
    ProfileOutlined,
    BarChartOutlined,
    UploadOutlined,
    VideoCameraOutlined,
} from '@ant-design/icons';

const linksSpecifications = [
    {
        label: 'سیگنال ها و سیستم ها',
        path: Routes.classLink(3452),
        key: '1',
        icon: <ProfileOutlined />,
    },
    {
        label: 'ریاضیات مهندسی',
        path: Routes.classLink(3452),
        key: '2',
        icon: <VideoCameraOutlined />,
    },
    {
        label: 'ریاضیات مهندسی',
        path: Routes.classLink(3452),
        key: '3',
        icon: <UploadOutlined />,
    },
    {
        label: 'سیگنال ها و سیستم ها',
        path: Routes.classLink(3452),
        key: '4',
        icon: <BarChartOutlined />,
    },
];

export default function ClassesLayoutContainer() {
    return (
        <Layout>
            <Sidebar links={linksSpecifications} defaultSelectedKey={'1'} />
            <LayoutView>
                <ClassesContainer />
            </LayoutView>
        </Layout>
    );
}
