import React, {useEffect, useState} from 'react';
import FormView from '../../Components/ExamForms/FormView';
import {notification, Modal} from 'antd';
import {withRouter} from 'react-router-dom';
import * as Routes from '../../Constants/Routes/Routes';
import {ExclamationCircleOutlined} from '@ant-design/icons';
import {createExam} from '../../Lib/ApiService/Private/index';
import ErrorHandler from '../../Lib/ErrorHandler';
import ResponseHandler from '../../Lib/ResponseHandler';
import LoadingOverlay from '../../Components/LoadingOverlay/LoadingOverlay';
import {useQuery} from '@apollo/react-hooks';
import {GET_QUESTIONS_BY_IDS} from '../../Lib/graphql/queries';
import {
    getExam,
    deleteQuestionFromExam,
    editExam,
    getExamQuestions,
} from '../../Lib/ApiService/Private';
import ComplexTable from '../../Components/ComplexTable/ComplexTable';

const {confirm} = Modal;

function CreateExamContainer(props) {
    const [editMode, setEditMode] = useState(false);
    const [btnText, setBtnText] = useState('ساختن آزمون جدید');
    const [loadingState, setLoadingState] = useState(true);
    const [examInfo, setExamInfo] = useState({});
    const [examQuestions, setExamQuestions] = useState([]);
    const classId = props.match.params.class_id;
    const examId = props.match.params.exam_id;
    const {refetch: getQuestionsByIds} = useQuery(GET_QUESTIONS_BY_IDS);

    useEffect(() => {
        if (examId) {
            fetchData(examId);
        } else {
            setLoadingState(false);
        }
    }, []);

    const getQuestionsDetail = async ids => {
        let idsArray = ids.map(item => item['question']);
        if (idsArray.length === 0) {
            return;
        }
        console.log(idsArray);
        const {data} = await getQuestionsByIds({
            variables: {ids: idsArray},
        });
        console.log(data);
        setExamQuestions(data);
        setLoadingState(false);
    };

    const fetchData = async examId => {
        setEditMode(true);
        setBtnText('ویرایش آزمون');
        await getExam(examId)
            .then(async response => {
                setExamInfo(response.data);
            })
            .catch(err => {
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
        await getExamQuestions(examId)
            .then(async response => {
                getQuestionsDetail(response.data);
            })
            .catch(err => {
                setLoadingState(false);
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
    };

    const createExamHandler = async vars => {
        if (vars.name && vars.begin_time) {
            if (editMode) {
                setLoadingState(true);
                editExam(examId, vars)
                    .then(response => {
                        let res = ResponseHandler(response);
                        setLoadingState(false);
                        notification[res.type]({
                            message: 'تغییرات با موفقیت اعمال شد',
                            description: res.serverMessage,
                            duration: 3,
                        });
                        window.history.back();
                    })
                    .catch(err => {
                        let res = ErrorHandler(err);
                        setLoadingState(false);
                        notification[res.type]({
                            message: <p dir={'rtl'}>{res.message}</p>,
                            description: <p dir={'rtl'}>{res.serverMessage}</p>,
                            duration: 3,
                        });
                    });
            } else {
                setLoadingState(true);
                createExam(vars)
                    .then(response => {
                        let res = ResponseHandler(response);
                        setLoadingState(false);
                        notification[res.type]({
                            message: 'امتحان با موفقیت ساخته شد.',
                            description: res.serverMessage,
                            duration: 3,
                        });
                        window.history.back();
                    })
                    .catch(err => {
                        let res = ErrorHandler(err);
                        setLoadingState(false);
                        notification[res.type]({
                            message: <p dir={'rtl'}>{res.message}</p>,
                            description: <p dir={'rtl'}>{res.serverMessage}</p>,
                            duration: 3,
                        });
                    });
            }
        }
    };

    const removeQuestionFromExam = qId => {
        setLoadingState(true);
        deleteQuestionFromExam(qId, examId)
            .then(async response => {
                setLoadingState(false);
                let res = ResponseHandler(response);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
                window.location.reload();
            })
            .catch(err => {
                setLoadingState(false);
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
    };
    const showConfirm = qId => {
        confirm({
            title: 'آیا مایل به حذف این سوال هستید؟',
            icon: <ExclamationCircleOutlined />,
            okText: 'بله',
            cancelText: 'انصراف',
            onOk() {
                return removeQuestionFromExam(qId);
            },
            onCancel() {},
        });
    };

    let editModeContent = (
        <div>
            <h3 className={'text_center'}>سوالات امتحان</h3>
            <hr />
            <ComplexTable
                loading={loadingState}
                data={examQuestions}
                // handleToggleModal={handleToggleModal}
                classId={props.match.params.class_id}
                actions={{
                    remove: ['حذف', showConfirm],
                }}
                type={'allQuestions'}
            />
        </div>
    );
    return (
        <LoadingOverlay
            active={loadingState}
            spinner
            fadeSpeed={100}
            text="در حال دریافت اطلاعات"
            styles={{backgroundColor: 'white'}}
        >
            <div style={{minHeight: '528px'}}>
                <h3 className={'text_center'}>{btnText}</h3>
                <hr />

                <FormView
                    btnText={btnText}
                    createExam={createExamHandler}
                    exam={examInfo}
                    editMode={examId}
                />

                <br />
                {editMode ? editModeContent : null}
            </div>
        </LoadingOverlay>
    );
}

export default withRouter(CreateExamContainer);
