import React, {useState, useEffect} from 'react';
import Header from '../../Components/Table/Header';
import TableView from '../../Components/Table/TableView';
import {Tag, Button, notification, Modal} from 'antd';
import {Link} from 'react-router-dom';
import {PlusCircleOutlined, ExclamationCircleOutlined} from '@ant-design/icons';
import * as Routes from '../../Constants/Routes/Routes';
import persianJs from 'persianjs';
import CustomModal from '../../Components/Modal/CustomModal';
import ErrorHandler from '../../Lib/ErrorHandler';
import {getAllExams, removeExam} from '../../Lib/ApiService/Private';
import LoadingOverlay from 'react-loading-overlay';
import moment from 'jalali-moment';
import ResponseHandler from '../../Lib/ResponseHandler';

const {confirm} = Modal;

const defaultValue = [];

export default function MyExams(props) {
    const [visible, toggleModal] = useState(false);
    const [qId, setQId] = useState(false);
    const [tableData, setTableData] = useState(defaultValue);
    const [loadingState, setLoadingState] = useState(true);
    const examTimeDuration = 3 * 60 * 60;

    const columns = [
        {
            title: 'امتحان',
            dataIndex: 'name',
            key: 'name',
            render: (text, record) => (
                <div>
                    {getTag(record.status)}
                    <p>
                        <Button type={'link'}>
                            <Link to={Routes.examLink(classId, record.key)}>
                                {text}
                            </Link>
                        </Button>
                    </p>
                </div>
            ),
        },
        {
            title: 'سازنده',
            dataIndex: 'creator',
            key: 'creator',
            render: text => (
                <div className={'text_center'}>
                    <p>{text}</p>
                </div>
            ),
        },
        {
            title: 'تاریخ برگزاری',
            dataIndex: 'holdDate',
            key: 'holdDate',
            render: text => (
                <div className={'text_center'}>
                    <p>{String(persianJs(text[1]).englishNumber())}</p>
                    <p>{String(persianJs(text[0]).englishNumber())}</p>
                </div>
            ),
        },
        {
            title: 'زمان امتحان',
            dataIndex: 'examTime',
            key: 'examTime',
            render: text => (
                <div className={'time_wrapper text_center'}>
                    <span>
                        <p>{String(persianJs(text[0]).englishNumber())}</p>
                        <p>دقیقه</p>
                    </span>
                    <span>
                        <p>{String(persianJs(text[1]).englishNumber())}</p>
                        <p>ساعت</p>
                    </span>
                </div>
            ),
        },
        {
            title: '',
            dataIndex: 'edit',
            key: 'edit',
            render: (action, record) => (
                <Link to={Routes.editExamLink(classId, record.key)}>
                    <Button
                        className={'table_button'}
                        size={'small'}
                        disabled={record.status === 'passed'}
                    >
                        {action}
                    </Button>
                </Link>
            ),
        },
        {
            title: '',
            dataIndex: 'enter',
            key: 'enter',
            render: (action, record) => (
                <Link to={Routes.examLink(classId, record.key)}>
                    <Button className={'table_button'} size={'small'}>
                        {action}
                    </Button>
                </Link>
            ),
        },
        {
            title: '',
            dataIndex: 'remove',
            key: 'remove',
            render: (action, record) => (
                <Button
                    className={'table_button'}
                    size={'small'}
                    onClick={() => showConfirm(record.key)}
                    // disabled={record.status === 'passed'}
                >
                    {action}
                </Button>
            ),
        },
    ];

    const classId = props.match.params.class_id;

    const parseData = data => {
        let {results} = data;
        console.log(results);
        let tableData = results.filter(exam => exam['is_public']);
        tableData = tableData.map(exam => {
            let {id, name, creator} = exam;
            let holdDate = exam['begin_time'].split('T');
            let examTime = ['0', '3'];
            let holdTimeStampObj = moment(exam['begin_time']);
            let momentTimeStampObj = moment();
            holdDate[0] = holdTimeStampObj.format('jYYYY/jMM/jDD');
            holdDate[1] = holdDate[1].slice(0, 8);
            let timeDiff =
                moment.duration(holdTimeStampObj.diff(momentTimeStampObj)) /
                    1000 -
                16200;
            let status;
            if (timeDiff > 0) {
                status = 'coming';
            } else if (timeDiff < 0 && timeDiff < -examTimeDuration) {
                status = 'passed';
            } else {
                status = 'running';
            }
            return {
                key: id,
                name: name,
                creator: creator,
                holdDate: holdDate,
                examTime: examTime,
                enter: 'ورود به امتحان',
                edit: 'ویرایش',
                status: status,
                remove: 'حذف امتحان',
            };
        });
        setTableData(tableData);
    };

    // todo : Handle pagination
    useEffect(() => {
        setLoadingState(true);
        getAllExams()
            .then(async response => {
                await parseData(response.data);
                setLoadingState(false);
            })
            .catch(err => {
                setLoadingState(false);
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
    }, []);

    const getTag = status => {
        switch (status) {
            case 'coming':
                return <Tag color={'#008274'}>پیش رو</Tag>;
            case 'passed':
                return <Tag color={'#008274'}>گذشته</Tag>;
            case 'running':
                return <Tag color={'#008274'}>در حال برگزاری</Tag>;
        }
    };

    const removeExamHandler = examId => {
        setLoadingState(true);
        removeExam(examId)
            .then(response => {
                let res = ResponseHandler(response);
                setLoadingState(false);
                notification[res.type]({
                    message: 'تغییرات با موفقیت اعمال شد.',
                    description: res.serverMessage,
                    duration: 3,
                });
                window.location.reload();
            })
            .catch(err => {
                let res = ErrorHandler(err);
                setLoadingState(false);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
    };

    const showConfirm = examId => {
        return confirm({
            title: 'آیا می خواهید این امتحان را حذف کنید؟',
            icon: <ExclamationCircleOutlined />,
            // content: 'با حذف کردن امتحان تمام اطلاعات مربوط به',
            okText: 'بله',
            cancelText: 'انصراف',
            onOk() {
                removeExamHandler(examId);
            },
            onCancel() {},
        });
    };

    const handleToggleModal = qId => {
        toggleModal(!visible);
        setQId(qId);
    };

    return (
        <LoadingOverlay
            active={loadingState}
            spinner
            fadeSpeed={100}
            text="در حال دریافت اطلاعات"
            styles={{backgroundColor: 'white'}}
        >
            <Link to={Routes.createExamLink(classId)}>
                <Button type="link">
                    ساختن آزمون جدید
                    <PlusCircleOutlined className={'mr-2'} />
                </Button>
            </Link>
            <Link to={Routes.myExamsLink(classId)}>
                <Button type="link">آزمون های من</Button>
            </Link>
            <hr />
            <Header title={'امتحانات من'} />
            <TableView
                columns={columns}
                pagination={false}
                dataSource={tableData}
            />
            <CustomModal
                visible={visible}
                data={{qId}}
                onClose={handleToggleModal}
                okText={'حذف ازمون'}
                cancelText={<p>انصراف</p>}
                handleOk={() => console.log('fetch data')}
            >
                <h3>حذف کلی آزمون</h3>
            </CustomModal>

            <br />
        </LoadingOverlay>
    );
}
