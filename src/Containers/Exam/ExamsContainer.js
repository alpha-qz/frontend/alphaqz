import React, {useEffect, useState} from 'react';
import Header from '../../Components/Table/Header';
import TableView from '../../Components/Table/TableView';
import {Link, withRouter} from 'react-router-dom';
import {PlusCircleOutlined} from '@ant-design/icons';
import {Button, notification} from 'antd';
import * as Routes from '../../Constants/Routes/Routes';
import persianJs from 'persianjs';
import {getAllExams} from '../../Lib/ApiService/Private';
import LoadingOverlay from '../../Components/LoadingOverlay/LoadingOverlay';
import ErrorHandler from '../../Lib/ErrorHandler';
import moment from 'jalali-moment';

moment.locale('en');
const defaultValue = {
    passed: [],
    coming: [],
    running: [],
};

function ExamsContainer(props) {
    const [tableData, setTableData] = useState(defaultValue);
    const [loadingState, setLoadingState] = useState(true);
    const classId = props.match.params.class_id;
    const parseData = data => {
        let {results} = data;
        let tableData = results.filter(exam => exam['is_public']);
        tableData = tableData.map(exam => {
            let {id, name, creator} = exam;
            let holdDate = exam['begin_time'].split('T');
            let examTime = ['0', '3'];
            let holdTimeStampObj = moment(exam['begin_time']);
            let momentTimeStampObj = moment();
            holdDate[0] = holdTimeStampObj.format('jYYYY/jMM/jDD');
            holdDate[1] = holdDate[1].slice(0, 8);
            let timeDiff =
                moment.duration(holdTimeStampObj.diff(momentTimeStampObj)) /
                    1000 -
                16200;
            let status;
            if (timeDiff > 0) {
                status = 'coming';
            } else if (timeDiff < 0 && timeDiff < -examTimeDuration) {
                status = 'passed';
            } else {
                status = 'running';
            }
            return {
                key: id,
                name: name,
                creator: creator,
                holdDate: holdDate,
                examTime: examTime,
                enter: 'ورود به امتحان',
                status: status,
            };
        });

        let passedExam = [];
        let comingExam = [];
        let runningExam = [];

        tableData.forEach(item => {
            switch (item['status']) {
                case 'running':
                    runningExam.push(item);
                    break;
                case 'passed':
                    passedExam.push(item);
                    break;
                case 'coming':
                    comingExam.push(item);
                    break;
            }
        });
        setTableData({
            passed: passedExam,
            coming: comingExam,
            running: runningExam,
        });
    };
    const examTimeDuration = 3 * 60 * 60;

    // todo : Handle pagination
    useEffect(() => {
        setLoadingState(true);
        getAllExams()
            .then(async response => {
                await parseData(response.data);
                setLoadingState(false);
            })
            .catch(err => {
                setLoadingState(false);
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
    }, []);

    const columns = [
        {
            title: 'امتحان',
            dataIndex: 'name',
            key: 'name',
            render: (text, record) => (
                <div>
                    <p>
                        <Button
                            type={'link'}
                            disabled={record.status === 'coming'}
                        >
                            <Link to={Routes.examLink(classId, record.key)}>
                                {text}
                            </Link>
                        </Button>
                    </p>
                </div>
            ),
        },
        {
            title: 'سازنده',
            dataIndex: 'creator',
            key: 'creator',
            render: text => (
                <div className={'text_center'}>
                    <p>{text}</p>
                </div>
            ),
        },
        {
            title: 'تاریخ برگزاری',
            dataIndex: 'holdDate',
            key: 'holdDate',
            render: text => (
                <div className={'text_center'}>
                    <p>{String(persianJs(text[1]).englishNumber())}</p>
                    <p>{String(persianJs(text[0]).englishNumber())}</p>
                </div>
            ),
        },
        {
            title: 'زمان امتحان',
            dataIndex: 'examTime',
            key: 'examTime',
            render: text => (
                <div className={'time_wrapper text_center'}>
                    <span>
                        <p>{String(persianJs(text[0]).englishNumber())}</p>
                        <p>دقیقه</p>
                    </span>
                    <span>
                        <p>{String(persianJs(text[1]).englishNumber())}</p>
                        <p>ساعت</p>
                    </span>
                </div>
            ),
        },
        {
            title: '',
            dataIndex: 'enter',
            key: 'enter',
            render: (action, record) => (
                <Link to={Routes.examLink(classId, record.key)}>
                    <Button
                        className={'table_button'}
                        size={'small'}
                        disabled={record.status === 'coming'}
                    >
                        {action}
                    </Button>
                </Link>
            ),
        },
    ];

    // const data = [
    //     {
    //         key: '1',
    //         id: '1',
    //         test: ['سیگنال ها و سیستم ها', 'میانترم دوم'],
    //         timeToStart: ['۲۶ اسفند ۱۳۹۸ ساعت ۱۶:۰۵', '۹ روز'],
    //         examTime: ['0', '3'],
    //         questionNumber: '۱۲',
    //         method: 'تشریحی',
    //         action: 'ورود به امتحان',
    //     },
    //     {
    //         key: '2',
    //         id: '2',
    //         test: ['سیگنال ها و سیستم ها', 'میانترم دوم'],
    //         timeToStart: ['۲۶ اسفند ۱۳۹۸ ساعت ۱۶:۰۵', '۹ روز'],
    //         examTime: ['0', '3'],
    //         questionNumber: '۱۲',
    //         method: 'تشریحی',
    //         action: 'ورود به امتحان',
    //     },
    //     {
    //         key: '3',
    //         id: '3',
    //         test: ['سیگنال ها و سیستم ها', 'میانترم دوم'],
    //         timeToStart: ['۲۶ اسفند ۱۳۹۸ ساعت ۱۶:۰۵', '۹ روز'],
    //         examTime: ['0', '3'],
    //         questionNumber: '۱۲',
    //         method: 'تشریحی',
    //         action: 'ورود به امتحان',
    //     },
    // ];

    return (
        <LoadingOverlay
            active={loadingState}
            spinner
            fadeSpeed={100}
            text="در حال دریافت اطلاعات"
            styles={{backgroundColor: 'white'}}
        >
            <Link to={Routes.createExamLink(classId)}>
                <Button type="link">
                    ساختن آزمون جدید
                    <PlusCircleOutlined className={'mr-2'} />
                </Button>
            </Link>
            <Link to={Routes.myExamsLink(classId)}>
                <Button type="link">آزمون های من</Button>
            </Link>
            <hr />
            <Header title={'امتحانات پیش رو'} />
            <TableView
                columns={columns}
                pagination={false}
                dataSource={tableData['coming']}
            />
            <br />
            <Header title={'امتحانات در حال برگزاری'} />
            <TableView
                columns={columns}
                pagination={false}
                dataSource={tableData['running']}
            />
            <br />
            <Header title={'امتحانات اخیر'} />
            <TableView
                columns={columns}
                pagination={false}
                dataSource={tableData['passed']}
            />
        </LoadingOverlay>
    );
}

export default withRouter(ExamsContainer);
