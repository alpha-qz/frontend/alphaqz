import React, {useEffect} from 'react';
import {useQuery} from '@apollo/react-hooks';
import {withRouter} from 'react-router-dom';
import Question from '../OnlineExam/Question';
import {GET_QUESTION_BY_ID} from '../../Lib/graphql/queries';
import LoadingOverlay from '../../Components/LoadingOverlay/LoadingOverlay';

function QuestionContainer(props) {
    let qId = props.match.params.question_id;

    let nodeId = `QuestionNode:${qId}`;
    const {loading, error, data, refetch} = useQuery(GET_QUESTION_BY_ID, {
        variables: {
            id: btoa(nodeId),
        },
    });

    useEffect(() => {
        refetch();
    }, []);

    let question = {};
    if (!loading && !error) {
        console.log(data);
        question['label'] = data.question.title;
        question['type'] = 'test';
        question['difficulty'] = data.question.difficulty;
        question['creator'] = data.question.createName;
        question['key'] = qId;
        question['description'] = data.question.questionText;
        let choiceSet = {...data.question.choiceset};
        delete choiceSet['id'];
        delete choiceSet['__typename'];
        question['options'] = choiceSet;
    }

    return (
        <LoadingOverlay
            active={loading}
            spinner
            text="در حال دریافت اطلاعات"
            styles={{backgroundColor: 'white'}}
        >
            <Question {...props} question={question} onlineTest={false} />
        </LoadingOverlay>
    );
}

export default withRouter(QuestionContainer);
