import React, {useState} from 'react';
import {withRouter, Link} from 'react-router-dom';
import * as Routes from '../../Constants/Routes/Routes';
import {Button, notification, Modal} from 'antd';
import '../../Components/specialStyles/QuestionTable.css';
import CustomModal from '../../Components/Modal/CustomModal';
import {useQuery} from '@apollo/react-hooks';
import {GET_MY_QUESTIONS} from '../../Lib/graphql/queries';
import {PlusCircleOutlined, ExclamationCircleOutlined} from '@ant-design/icons';
import ComplexTable from '../../Components/ComplexTable/ComplexTable';
import AddQuestion from '../../Components/Question/AddQuestion';
import {getUsersExams, addQuestions} from '../../Lib/ApiService/Private';
import ErrorHandler from '../../Lib/ErrorHandler';
import ResponseHandler from '../../Lib/ResponseHandler';
import getToken from '../../Lib/getToken';

const {confirm} = Modal;

function MyQuestions(props) {
    const [visible, toggleModal] = useState(false);
    const [qId, setQId] = useState(false);
    const [loadingGetExams, setLoadingGetExams] = useState(true);
    const [examList, setExamList] = useState([]);
    const [modalSpec, setModalSpec] = useState({
        okText: 'حذف سوال',
        header: 'حذف سوال برای همیشه',
    });
    const token = getToken();
    const {loading, error, data} = useQuery(GET_MY_QUESTIONS, {
        variables: {token},
        errorPolicy: 'ignore',
        fetchPolicy: 'network-only',
    });
    const classId = props.match.params.class_id;

    const handleToggleModal = qId => {
        if (examList.length === 0) {
            getUsersExams()
                .then(response => {
                    setExamList(response.data.results);
                    setLoadingGetExams(false);
                })
                .catch(err => {
                    setLoadingGetExams(false);
                    let res = ErrorHandler(err);
                    notification[res.type]({
                        message: <p dir={'rtl'}>{res.message}</p>,
                        description: <p dir={'rtl'}>{res.serverMessage}</p>,
                        duration: 3,
                    });
                });
        }

        toggleModal(!visible);
        setQId(qId);
    };

    const addQuestionToExam = (qId, examId, point) => {
        let data = {
            question: qId,
            points: point,
        };
        setLoadingGetExams(true);
        addQuestions(examId, data)
            .then(response => {
                setLoadingGetExams(false);
                let res = ResponseHandler(response);
                notification[res.type]({
                    message: 'سوال با موفقیت به امتحان اضافه شد.',
                    description: res.serverMessage,
                    duration: 3,
                });
            })
            .catch(err => {
                setLoadingGetExams(false);
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
    };

    const editQuestion = qId =>
        props.history.push(Routes.editQuestionLink(classId, qId));

    const showConfirm = qId => {
        confirm({
            title: 'آیا مایل به حذف این سوال هستید؟',
            icon: <ExclamationCircleOutlined />,
            okText: 'بله',
            cancelText: 'انصراف',
            onOk() {
                return new Promise((resolve, reject) => {
                    setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
                }).catch(() => console.log('Oops errors!'));
            },
            onCancel() {},
        });
    };
    return (
        <>
            <Link to={Routes.createQuestionLink(classId)}>
                <Button type="link">
                    اضافه کردن سوال جدید
                    <PlusCircleOutlined className={'mr-2'} />
                </Button>
            </Link>
            <Link to={Routes.myQuestionsLink(classId)}>
                <Button type="link">سوالات من</Button>
            </Link>
            <hr />
            <ComplexTable
                error={error}
                loading={loading}
                data={data}
                handleToggleModal={handleToggleModal}
                classId={props.match.params.class_id}
                actions={{
                    addToExam: ['اضافه کردن به امتحان', handleToggleModal],
                    remove: ['حذف', showConfirm],
                    edit: ['ویرایش', editQuestion],
                }}
                type={'myQuestions'}
            />
            <CustomModal
                visible={visible}
                data={{qId}}
                onClose={handleToggleModal}
                okText={modalSpec.okText}
                cancelText={<p>انصراف</p>}
                handleOk={() => console.log('fetch data')}
            >
                <h3>{modalSpec.header}</h3>
            </CustomModal>
            <CustomModal
                visible={visible}
                onClose={handleToggleModal}
                cancelText={<p>بستن پنجره</p>}
                footer={[
                    <Button key="back" onClick={handleToggleModal}>
                        بستن پنجره
                    </Button>,
                ]}
            >
                <AddQuestion
                    examsList={examList}
                    addQuestion={addQuestionToExam}
                    loading={loadingGetExams}
                    questionId={qId}
                />
            </CustomModal>
        </>
    );
}

export default withRouter(MyQuestions);
