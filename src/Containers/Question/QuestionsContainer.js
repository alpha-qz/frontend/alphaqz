import React, {useState} from 'react';
import {withRouter, Link} from 'react-router-dom';
import * as Routes from '../../Constants/Routes/Routes';
import {Button, notification} from 'antd';
import '../../Components/specialStyles/QuestionTable.css';
import CustomModal from '../../Components/Modal/CustomModal';
import {useQuery, useLazyQuery} from '@apollo/react-hooks';
import {FILTER_QUESTIONS, GET_ALL_QUESTION} from '../../Lib/graphql/queries';
import {PlusCircleOutlined} from '@ant-design/icons';
import ComplexTable from '../../Components/ComplexTable/ComplexTable';
import InlineForm from '../../Components/InlineForm/InlineForm';
import AddQuestion from '../../Components/Question/AddQuestion';
import {getUsersExams, addQuestions} from '../../Lib/ApiService/Private';
import ErrorHandler from '../../Lib/ErrorHandler';
import ResponseHandler from '../../Lib/ResponseHandler';

function QuestionsContainer(props) {
    const [visible, toggleModal] = useState(false);
    const [loadingGetExams, setLoadingGetExams] = useState(true);
    const [filterState, setFilterState] = useState(false);
    const [examList, setExamList] = useState([]);
    const [qId, setQId] = useState(false);

    const {loading, error, data} = useQuery(GET_ALL_QUESTION, {
        errorPolicy: 'ignore',
        fetchPolicy: 'network-only',
    });

    const [
        filterQuestion,
        {loading: filterLoading, data: filteredData},
    ] = useLazyQuery(FILTER_QUESTIONS);

    const classId = props.match.params.class_id;

    const handleToggleModal = async qId => {
        toggleModal(!visible);
        setQId(qId);
        if (examList.length === 0) {
            getUsersExams()
                .then(response => {
                    setExamList(response.data.results);
                    setLoadingGetExams(false);
                })
                .catch(err => {
                    setLoadingGetExams(false);
                    let res = ErrorHandler(err);
                    notification[res.type]({
                        message: <p dir={'rtl'}>{res.message}</p>,
                        description: <p dir={'rtl'}>{res.serverMessage}</p>,
                        duration: 3,
                    });
                });
        }
    };
    const addQuestionToExam = (qId, examId, point) => {
        let data = {
            question: qId,
            points: point,
        };
        setLoadingGetExams(true);
        addQuestions(examId, data)
            .then(response => {
                setLoadingGetExams(false);
                let res = ResponseHandler(response);
                notification[res.type]({
                    message: 'سوال با موفقیت به امتحان اضافه شد.',
                    description: res.serverMessage,
                    duration: 3,
                });
            })
            .catch(err => {
                setLoadingGetExams(false);
                let res = ErrorHandler(err);
                notification[res.type]({
                    message: <p dir={'rtl'}>{res.message}</p>,
                    description: <p dir={'rtl'}>{res.serverMessage}</p>,
                    duration: 3,
                });
            });
    };

    const handleFilterData = async data => {
        setFilterState(!filterState);
        filterQuestion({variables: data});
    };

    let forms = [
        {
            key: 0,
            name: 'title',
            required: false,
            message: '',
            type: 'text',
            size: 'large',
            placeholder: 'جستجو در عنوان',
        },
        {
            key: 1,
            name: 'cat',
            required: false,
            message: '',
            type: 'text',
            size: 'large',
            placeholder: 'موضوع سوال',
        },
        {
            key: 2,
            name: 'tag',
            required: false,
            message: '',
            type: 'text',
            size: 'large',
            placeholder: 'تگ سوال',
        },
        {
            key: 3,
            name: 'questionText',
            required: false,
            message: '',
            type: 'text',
            size: 'large',
            placeholder: 'جستجو در متن سوال',
        },
    ];
    return (
        <>
            <Link to={Routes.createQuestionLink(classId)}>
                <Button type="link">
                    اضافه کردن سوال جدید
                    <PlusCircleOutlined className={'mr-2'} />
                </Button>
            </Link>
            <Link to={Routes.myQuestionsLink(classId)}>
                <Button type="link">سوالات من</Button>
            </Link>
            <hr />
            <InlineForm
                forms={forms}
                handleFilterData={handleFilterData}
                loading={filterLoading}
                filterState={filterState}
            />
            <ComplexTable
                error={error}
                loading={loading || filterLoading}
                data={filterState && filteredData ? filteredData : data}
                classId={props.match.params.class_id}
                actions={{
                    addToExam: ['اضافه کردن به امتحان', handleToggleModal],
                }}
                type={'allQuestions'}
            />
            <CustomModal
                visible={visible}
                onClose={handleToggleModal}
                cancelText={<p>بستن پنجره</p>}
                footer={[
                    <Button key="back" onClick={handleToggleModal}>
                        بستن پنجره
                    </Button>,
                ]}
            >
                <AddQuestion
                    examsList={examList}
                    addQuestion={addQuestionToExam}
                    loading={loadingGetExams}
                    questionId={qId}
                />
            </CustomModal>
        </>
    );
}

export default withRouter(QuestionsContainer);
