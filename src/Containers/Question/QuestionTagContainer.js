import React from 'react';
import {withRouter} from 'react-router-dom';
import {Select} from 'antd';
import {useQuery} from '@apollo/react-hooks';
import Spinner from "../../Components/UI's/Spin/Spin";
import {GET_ALL_TAGS, GET_ALL_CATS} from '../../Lib/graphql/queries';

const {Option} = Select;
const type = [GET_ALL_TAGS, GET_ALL_CATS];
function QuestionsTagContainer(props) {
    const query = props.type === 'tag' ? type[0] : type[1];
    const {loading, error, data} = useQuery(query);
    const setChildren = () => {
        if (error) return [];
        if (props.type === 'tag') {
            return data.allTags.edges.map(node => (
                <Option key={node.node['title']}>{node.node['title']}</Option>
            ));
        } else {
            return data.allCategories.edges.map(node => (
                <Option key={node.node['title']}>{node.node['title']}</Option>
            ));
        }
    };

    const onChange = value => {
        props.getInputValue(value);
    };

    return (
        <>
            <Select
                mode="tags"
                placeholder={'تگ سوالات مورد نظر را وارد کنید'}
                size={'large'}
                maxTagCount={props.maxTagCount}
                onChange={onChange}
                defaultValue={props.defaultValue}
                notFoundContent={<Spinner />}
                style={{
                    width: '100%',
                    height: '20px',
                    marginBottom: '20px',
                    fontSize: '12px',
                }}
                value={props.value}
            >
                {loading ? null : setChildren()}
            </Select>
        </>
    );
}
QuestionsTagContainer.defaultValue = {
    type: 'tag',
};
export default withRouter(QuestionsTagContainer);
