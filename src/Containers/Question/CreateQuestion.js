import React, {useEffect, useState} from 'react';
import FormView from '../../Components/QuestionForms/FormView';
import {withRouter} from 'react-router-dom';
import {useMutation, useLazyQuery} from '@apollo/react-hooks';
import {
    CREATE_QUESTION,
    EDIT_QUESTION,
    GET_QUESTION_BY_ID,
} from '../../Lib/graphql/queries';
import LoadingOverlay from '../../Components/LoadingOverlay/LoadingOverlay';
import {notification} from 'antd';
import getToken from '../../Lib/getToken';
import * as Routes from '../../Constants/Routes/Routes';

function CreateQuestion(props) {
    let qId = props.match.params.question_id;
    let nodeId = `QuestionNode:${qId}`;
    let classId = props.match.params.class_id;
    const [addQuestion, {loading: addQuestionLoading}] = useMutation(
        CREATE_QUESTION,
    );
    const [editQuestion, {loading: editQuestionLoading}] = useMutation(
        EDIT_QUESTION,
    );
    const [editMode, setEditMode] = useState(false);
    const createQuestion = async vars => {
        let token = getToken();
        if (
            vars.title &&
            vars.questionText &&
            vars.difficulty &&
            vars.rightChoice &&
            vars.choiceset.length &&
            vars.categories.length &&
            vars.tags.length
        ) {
            if (editMode) {
                try {
                    vars = {...vars, token};
                    await editQuestion({variables: {...vars}});
                    props.history.push(Routes.questionsLink(classId));
                } catch (err) {
                    // console.log(err);
                }
            } else {
                vars = {...vars, token};
                await addQuestion({variables: {...vars}});
                props.history.push(Routes.questionsLink(classId));
            }

            return notification['success']({
                message: 'عملیات با موفقیت انجام شد',
            });
        } else {
            return notification['warning']({
                message: 'مشکلی پیش آمده است',
                description:
                    'مطمئن شوید که آیا تمام فیلد ها را مقدار دهی کرده اید.',
            });
        }
    };

    const [
        getQuestion,
        {loading: getQuestionLoading, error, data},
    ] = useLazyQuery(GET_QUESTION_BY_ID, {
        fetchPolicy: 'network-only',
    });

    useEffect(() => {
        if (qId) {
            setEditMode(true);
            getQuestion({
                variables: {
                    id: btoa(nodeId),
                },
            });
        }
    }, []);

    let question = {};
    if (data && data.question) {
        if (!getQuestionLoading && !error) {
            console.log(data);
            question['id'] = parseInt(qId);
            question['label'] = data.question.title;
            question['type'] = 'test';
            question['difficulty'] = data.question.difficulty;
            question['creator'] = data.question.createName;
            question['key'] = qId;
            question['description'] = data.question.questionText;
            question['tag'] = data.question.qtag.edges.map(
                node => node.node.tag.title,
            );
            question['cat'] = data.question.qcategory.edges.map(
                node => node.node.category.title,
            );
            question['isPublic'] = data.question.isPublic;
            let choiceSet = {...data.question.choiceset};
            delete choiceSet['id'];
            delete choiceSet['__typename'];
            question['options'] = choiceSet;
        }
    }
    //Todo : Refactor react-overlay component
    return (
        <div>
            <LoadingOverlay
                active={
                    addQuestionLoading ||
                    getQuestionLoading ||
                    editQuestionLoading
                }
                spinner
                text="در حال دریافت اطلاعات"
                styles={{backgroundColor: 'white'}}
            >
                {addQuestionLoading || getQuestionLoading ? null : (
                    <>
                        <h3 className={'text_center'}>اضافه کردن سوال جدید</h3>
                        <hr />
                        <FormView
                            createQuestion={createQuestion}
                            question={question}
                            editMode={editMode}
                        />
                    </>
                )}
            </LoadingOverlay>
        </div>
    );
}

export default withRouter(CreateQuestion);
