import React, {Component} from 'react';
import AuthService from './Lib/AuthService/AuthService';
import LoadingOverlay from './Components/LoadingOverlay/LoadingOverlay';
import {withRouter, Route, Switch} from 'react-router-dom';
import * as Routes from './Constants/Routes/Routes';
import ClassesLayoutContainer from './Containers/Class/ClassesLayoutContainer';
import MainLayoutContainer from './Containers/MainLayoutContainer';
import OnlineExamLayoutContainer from './Containers/OnlineExam/OnlineExamLayoutContainer';
import {ApolloProvider} from '@apollo/react-hooks';
import {client} from './Lib/graphql/config';

class App extends Component {
    constructor(props) {
        super(props);
        this.auth = new AuthService();
        this.state = {
            checkAuthStatus: true,
        };
    }

    componentDidMount() {
        this.onRouteChanged();
        this.unlisten = this.props.history.listen((location, action) => {
            console.log('route has changed');
            this.setState({
                checkAuthStatus: true,
            });
            this.onRouteChanged();
        });
    }

    componentWillUnmount() {
        this.unlisten();
    }

    onRouteChanged = async () => {
        let status = await this.auth.tokenIsValid();
        if (!status) {
            return this.redirectToTarget(Routes.authLink);
        }
        setTimeout(
            () =>
                this.setState({
                    checkAuthStatus: false,
                }),
            500,
        );
    };

    redirectToTarget = target => {
        this.props.history.push(target);
        window.location.reload();
    };

    render() {
        return (
            <LoadingOverlay
                fadeSpeed={200}
                active={this.state.checkAuthStatus}
                spinner
                text="در حال دریافت اطلاعات"
                styles={{backgroundColor: 'white'}}
            >
                <ApolloProvider client={client}>
                    <Switch>
                        <Route
                            exact
                            path={Routes.classesRoute}
                            component={ClassesLayoutContainer}
                        />
                        <Route
                            path={Routes.examRoute}
                            component={OnlineExamLayoutContainer}
                        />
                        <Route
                            path={Routes.classRoute}
                            component={MainLayoutContainer}
                        />
                    </Switch>
                </ApolloProvider>
            </LoadingOverlay>
        );
    }
}

export default withRouter(App);
